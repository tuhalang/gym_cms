import {useEffect} from "react"
import {useDispatch, useSelector} from "react-redux";
import {listCategories} from "../../redux/actions";
import {UPDATE_FOLDER_CATEGORY} from "../../redux/actions/action_types";

export const ROOT_FOLDER_CATEGORY = {nameEn: "Category", id: null, path: []}

export default function useCategory({}) {

    const dispatch = useDispatch()

    const state = useSelector(state => state.category)
    // useEffect(() => {
    //     dispatch({type: SELECT_FOLDER, payload: {folder, folderId}})
    // }, [folderId, folder])

    useEffect(() => {
        if (state.folderId == null) {
            return dispatch({
                type: UPDATE_FOLDER_CATEGORY,
                payload: {folder: ROOT_FOLDER_CATEGORY}
            })
        }

        // database.folders
        //     .doc(folderId)
        //     .get()
        //     .then(doc =>
        //         dispatch({
        //             type: ACTIONS.UPDATE_FOLDER,
        //             payload: { folder: database.formatDoc(doc) }
        //         })
        //     )
        //     .catch(() =>
        //         dispatch({
        //             type: ACTIONS.UPDATE_FOLDER,
        //             payload: { folder: ROOT_FOLDER_CATEGORY }
        //         })
        //     )
    }, [state.folderId])

    // useEffect(() => {
    //     dispatch(listAsset({
    //         parentId: null,
    //         name: '',
    //         page: 1,
    //         size: 10,
    //         status: 1,
    //     }))
    // }, [])

    useEffect(() => {
        dispatch(listCategories({
            parentId: state.folderId,
            name: '',
            page: 1,
            size: 10,
            status: 1,
        }))
    }, [state.folderId])

    return state
}
