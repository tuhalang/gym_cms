package com.viettel.gym_cms.repo;

import com.viettel.gym_cms.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, String>, CommonRepo {

    User findByUserName(String userName);
    boolean existsByUserName(String userName);

    @Query(
            "select u from User u where upper(u.userName) like lower(concat('%', :key, '%')) " +
                    "and upper(u.role) like lower(concat(:role, '%'))"
    )
    Page<User> findAllByKey(Pageable pageable, String key, String role);

    @Query(
            "select u from User u where u.status = :status and upper(u.userName) like lower(concat('%', :key, '%')) " +
                    "and upper(u.role) like lower(concat(:role, '%'))"
    )
    Page<User> findAllByKeyAndStatus(Pageable pageable, String key, Integer status, String role);


}
