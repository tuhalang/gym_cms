import React from "react";
import {DNS_IMAGE} from "../../utils/constant";
import {Avatar, Image, List, Skeleton} from "antd";
import Preview from "../modal/Preview";

export default function File({file}) {
    const [preview, setPreview] = React.useState('')

    const renderPreview = ({file}) => {
        const {link, name} = file
        let url = DNS_IMAGE + link
        if (name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".jpeg")) {
            return (
                <Image src={url} className="preview"/>
            )
        } else if (name.endsWith('.mp4') || name.endsWith('.webm') || name.endsWith('.gif') || name.endsWith('.wav')) {
            return (
                <i className='bx bxs-videos preview' onClick={() => setPreview(url)}></i>
            )
        } else {
            return (
                <i className='bx bxs-confused preview'></i>
            )
        }
    }

    return (
        <
        div
        className = "flex items-center flex-grow file" >
            < List.Item.Meta
        avatar = {
            renderPreview({file})
        }
        title = {
        <span>{file.name}</span>
    }
        description = {
        <span className="sub-title">{DNS_IMAGE + file.link}</span>
    }
        />
        <div className="p-2">{file.createdBy}</div>
        <div className="p-2">{file.createdAt}</div>
        <Preview visible={preview && preview != ''} onClose={() => setPreview('')} url={preview}/>
    </div>
    );
    }
