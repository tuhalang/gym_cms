import React, {Suspense, useEffect, Fragment} from "react"
import {
    Route,
    Switch,
    Redirect,
    Link, HashRouter,
} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux"
import {Result} from "antd"
import Layout from "./components/layout/Layout";
import {getMenus} from "./redux/actions";
import SuspenseComponent from "./components/suspense/SuspenseComponent";
import Users from "./pages/User/Users";
import Login from "./pages/Common/Login";
import Register from "./pages/Common/Register"
import EditNews from "./pages/EditNews";
import Categories from "./pages/Categories/Categories";
import Asset from "./pages/Asset/Asset";
import Lesson from "./pages/Lesson"
import Unit from "./pages/Unit"
import Trainer from "./pages/Trainer"
// import ModalEvent from "components/Modal/ModalEvent"
// import Verify from "pages/auth/Verify"
// import ForgotPassword from "pages/auth/ForgotPassword"
// import ChangePassword from "pages/auth/ChangePassword"
//
// import RegisterUsel from "pages/auth/RegisterUsel"
// import Layout from "components/Layout/Layout"

// const User = React.lazy(() => import("pages/User"));

function PrivateRoute({children, ...rest}) {
    const checkToken = localStorage.getItem("accessToken")
    return (
        <Route
            {...rest}
            render={() => (checkToken ? children : <Redirect to="/login"/>)}
        />
    )
}

const ProtectedPage = () => {
    const dispatch = useDispatch()
    const {menus} = useSelector(state => state.user)

    function WaitingComponent(Component, code) {
        if (menus.find((i) => i.code === code)) {
            return (props) => (
                <Suspense fallback={<SuspenseComponent/>}>
                    <Component {...props} />
                </Suspense>
            )
        } else {
            return NotLoadAuthorization
        }
    }

    useEffect(() => {
        dispatch(getMenus())
    }, [])

    return (
        <Fragment>
            {/* <ModalNewyear /> */}
            {/* <ModalEvent /> */}
            {/*{menus.length > 0 ? (*/}
            <Layout>
                <Switch>
                    <Route
                        path="/users"
                        exact
                        component={WaitingComponent(Users, "CUSTOMERS")}
                    />
                    <Route
                        path="/edit"
                        exact
                        component={WaitingComponent(EditNews, "EDIT")}
                    />
                    <Route
                        path="/categories"
                        exact
                        component={WaitingComponent(Categories, "CATEGORIES")}
                    />
                    <Route
                        exact
                        path="/register"
                        component={WaitingComponent(Register, "REGISTER")}/>
                    <Route
                        path="/asset"
                        exact
                        component={WaitingComponent(Asset, "ASSET")}
                    />
                    <Route
                        path="/lesson"
                        exact
                        component={WaitingComponent(Lesson, "LESSON")}
                    />
                    <Route
                        path="/unit"
                        exact
                        component={WaitingComponent(Unit, "UNIT")}
                    />
                    <Route
                        path="/trainer"
                        exact
                        component={WaitingComponent(Trainer, "TRAINER")}
                    />
                    <Route
                        path="/edit/:id"
                        exact
                        component={WaitingComponent(EditNews, "EDIT")}
                    />
                    {/*<Route*/}
                    {/*    path="/"*/}
                    {/*    exact*/}
                    {/*    component={WaitingComponent(Dashboard, "DASHBOARD")}*/}
                    {/*/>*/}
                    <Redirect from="/" to={"/users"}/>
                    <Route component={NotLoadAuthorization}/>
                </Switch>
            </Layout>
            {/*) : (*/}
            {/*    // <SpinnerLoadMenu />*/}
            {/*    <div/>*/}
            {/*)}*/}
        </Fragment>
    )
}

const Routes = () => {
    return (
        <HashRouter>
            <Switch>
                <Route exact path="/login" component={Login}/>
                {/*<Route exact path="/verify" component={Verify} />*/}
                {/*<Route exact path="/forgot-password" component={ForgotPassword} />*/}
                {/*<Route exact path="/reset-password" component={ChangePassword} />*/}
                <PrivateRoute path="/">
                    <ProtectedPage/>
                </PrivateRoute>
            </Switch>
        </HashRouter>
    )
}

export default Routes

const NotLoadAuthorization = () => {
    const themeReducer = useSelector(state => state.ThemeReducer)

    return (
        <Result
            status="404"
            title={<span className={themeReducer.result}>404</span>}
            subTitle={<span className={themeReducer.result}>Trang không tồn tại</span>}
            extra={<Link to="/">Trở về trang chủ</Link>}
        />
    )
}
