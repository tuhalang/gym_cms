package com.viettel.gym_cms.repo;

import com.viettel.gym_cms.domain.Unit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitRepo extends JpaRepository<Unit, String>, CommonRepo {

    Page<Unit> findByNameEnContainingIgnoreCaseAndStatus(Pageable pageable, String name, Integer status);
    Page<Unit> findByNameEnContainingIgnoreCaseAndStatusAndLessonId(Pageable pageable, String name, Integer status, String lessonId);

    Page<Unit> findByNameEnContainingIgnoreCase(Pageable pageable, String name);
    Page<Unit> findByNameEnContainingIgnoreCaseAndLessonId(Pageable pageable, String name, String lessonId);
}
