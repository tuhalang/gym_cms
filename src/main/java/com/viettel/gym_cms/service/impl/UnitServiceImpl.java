package com.viettel.gym_cms.service.impl;

import com.viettel.gym_cms.domain.Unit;
import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.repo.UnitRepo;
import com.viettel.gym_cms.service.UnitService;
import com.viettel.gym_cms.utils.Constant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@RequiredArgsConstructor
@Slf4j
@Service
public class UnitServiceImpl implements UnitService {

    private final UnitRepo unitRepo;

    @Override
    public ResponseDto getAll(Pageable pageable, Integer status, String name, String lessonId) {
        Page<Unit> page;
        if(name == null){
            name = "";
        }
        if(status == null){
            if(ObjectUtils.isEmpty(lessonId)) {
                page = unitRepo.findByNameEnContainingIgnoreCase(pageable, name);
            }else{
                page = unitRepo.findByNameEnContainingIgnoreCaseAndLessonId(pageable, name, lessonId);
            }
        }else{
            if(ObjectUtils.isEmpty(lessonId)) {
                page = unitRepo.findByNameEnContainingIgnoreCaseAndStatus(pageable, name, status);
            }else{
                page = unitRepo.findByNameEnContainingIgnoreCaseAndStatusAndLessonId(pageable, name, status, lessonId);
            }
        }

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("items", page.getContent())
                .onPushData("totalElements", page.getTotalElements())
                .onPushData("totalPages", page.getTotalPages())
                .build();
    }

    @Override
    public ResponseDto update(Unit unit) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        Unit oldUnit = unitRepo.findById(unit.getId()).orElse(null);
        if(oldUnit != null){
            if(!ObjectUtils.isEmpty(unit.getNameEn()) && !unit.getNameEn().equals(oldUnit.getNameEn())){
                oldUnit.setNameEn(unit.getNameEn());
            }

            if(!ObjectUtils.isEmpty(unit.getNameLc()) && !unit.getNameLc().equals(oldUnit.getNameLc())){
                oldUnit.setNameLc(unit.getNameLc());
            }

            if(!ObjectUtils.isEmpty(unit.getContentEn()) && !unit.getContentEn().equals(oldUnit.getContentEn())){
                oldUnit.setContentEn(unit.getContentEn());
            }

            if(!ObjectUtils.isEmpty(unit.getContentLc()) && !unit.getContentLc().equals(oldUnit.getContentLc())){
                oldUnit.setContentLc(unit.getContentLc());
            }

            if(!ObjectUtils.isEmpty(unit.getEstimateTime()) && !unit.getEstimateTime().equals(oldUnit.getEstimateTime())){
                oldUnit.setEstimateTime(unit.getEstimateTime());
            }

            if(!ObjectUtils.isEmpty(unit.getOrderNumber()) && !unit.getOrderNumber().equals(oldUnit.getOrderNumber())){
                oldUnit.setOrderNumber(unit.getOrderNumber());
            }

            if(!ObjectUtils.isEmpty(unit.getStatus()) && !unit.getStatus().equals(oldUnit.getStatus())){
                oldUnit.setStatus(unit.getStatus());
            }

            if(!ObjectUtils.isEmpty(unit.getVideoUrlEn()) && !unit.getVideoUrlEn().equals(oldUnit.getVideoUrlEn())){
                oldUnit.setVideoUrlEn(unit.getVideoUrlEn());
            }

            if(!ObjectUtils.isEmpty(unit.getVideoUrlLc()) && !unit.getVideoUrlLc().equals(oldUnit.getVideoUrlLc())){
                oldUnit.setVideoUrlLc(unit.getVideoUrlLc());
            }

            oldUnit.setUpdatedAt(unitRepo.currentDate());
            oldUnit.setUpdatedBy(userActive.getUserName());
            unitRepo.save(oldUnit);
        }else{
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_CANNOT_UPDATE)
                    .build();
        }
        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }

    @Override
    public ResponseDto create(Unit unit) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        unit.setId(unitRepo.getID());
        unit.setCreatedAt(unitRepo.currentDate());
        unit.setCreatedBy(userActive.getUserName());
        unit.setStatus(Constant.STATUS_ACTIVE);

        unitRepo.save(unit);

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }
}
