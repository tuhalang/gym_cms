package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.AssetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class AssetController {

    private final AssetService assetService;

    @RequestMapping(value = "/asset", method = RequestMethod.POST)
    public ResponseDto upload(@RequestParam(value = "parentId", required = false) String parentId,
                              @RequestParam(value = "type") Integer type,
                              @RequestParam(value = "name") String name,
                              @RequestParam(value = "file", required = false) MultipartFile file){
        return assetService.upload(parentId, type, name, file);
    }

    @RequestMapping(value = "/asset", method = RequestMethod.GET)
    public ResponseDto retrieve(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(value = "parentId", required = false) String parentId,
                                @RequestParam(value = "name", required = false) String name,
                                @RequestParam(value = "status", required = false) Integer status){
        Pageable pageable = PageRequest.of(page-1, size);
        return assetService.retrieve(pageable, parentId, name, status);
    }

    @RequestMapping(value = "/asset", method = RequestMethod.DELETE)
    public ResponseDto delete(@RequestParam(value = "id", required = true) String id){
        return assetService.delete(id);
    }
}
