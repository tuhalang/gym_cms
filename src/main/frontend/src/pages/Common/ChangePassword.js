// import React, { useState } from "react"
// import { Form, Input, Button, Row, Col, notification } from "antd"
// import { useHistory, Link } from "react-router-dom"
// import getFactory from "api"
// import queryString from "query-string"
// import wordwideBg from "images/wordwide.svg"
// import "./index.scss"
//
// const apiUser = getFactory("user")
// const Login = () => {
//   const [loading, setLoading] = useState(false)
//   const history = useHistory()
//   const { token } = queryString.parse(window.location.search)
//
//   async function onFinish(values) {
//     try {
//       setLoading(true)
//       const { message } = await apiUser.setPassword({ ...values, token })
//       notification.success({
//         message: "Notification",
//         description: message,
//         onClose: () => history.push("/signin"),
//       })
//     } catch (error) {
//       console.log(error)
//     } finally {
//       setLoading(false)
//     }
//   }
//
//   function onFinishFailed() {}
//   return (
//     <Row className="page-container vcs-page">
//       <Col xs={24} sm={12} md={12} lg={10}>
//         <div className="logo">
//           <div className="logo-vcs" />
//           <div className="logo-text"> EXPRESS </div>
//         </div>
//         <div className="page_login">
//           <div className="login_form is-floating-label">
//             <div className="text4xl f-500 mb-4">Quên mật khẩu</div>
//             <Form
//               name="basic"
//               layout="vertical"
//               initialValues={{ remember: true }}
//               onFinish={onFinish}
//               onFinishFailed={onFinishFailed}
//             >
//               <Form.Item
//                 label="Password"
//                 name="password"
//                 rules={[
//                   { required: true, message: "Please input your password!" },
//                 ]}
//               >
//                 <Input.Password />
//               </Form.Item>
//
//               <Form.Item
//                 label="Confirm password"
//                 name="confirm_password"
//                 dependencies={["password"]}
//                 rules={[
//                   {
//                     required: true,
//                     message: "Please input your confirm password!",
//                   },
//                   ({ getFieldValue }) => ({
//                     validator(_, value) {
//                       if (!value || getFieldValue("password") === value) {
//                         return Promise.resolve()
//                       }
//                       return Promise.reject(
//                         new Error(
//                           "The two passwords that you entered do not match!"
//                         )
//                       )
//                     },
//                   }),
//                 ]}
//               >
//                 <Input.Password />
//               </Form.Item>
//               <Form.Item>
//                 <Button
//                   className="w-right vcs-button w-full"
//                   type="primary"
//                   htmlType="submit"
//                   loading={loading}
//                 >
//                   Tạo mới mật khẩu
//                 </Button>
//               </Form.Item>
//               <div className="flex  justify-end">
//                 Hoặc &nbsp;<Link to="/signup"> Đăng ký </Link>
//               </div>
//             </Form>
//           </div>
//         </div>
//       </Col>
//       <Col xs={0} sm={12} md={12} lg={14}>
//         <div className="page_info">
//           <img className="page_img" alt="" src={wordwideBg} />
//           <div className="container">
//             <h1>Vận chuyển hàng e-commerce toàn cầu</h1>
//             <p>
//               VCS cung cấp dịch vụ vận chuyển e-commerce từ Việt Nam, Trung Quốc
//               tới tất cả các nước trên thế giới dù chỉ là một đơn hàng. Hàng hoá
//               có thể đi thẳng từ nhà sản xuất tới tận tay người tiêu dùng.
//             </p>
//           </div>
//         </div>
//       </Col>
//     </Row>
//   )
// }
//
// export default Login
