import React from "react";
import {Table, Tag, Tooltip, Popconfirm} from "antd";
import {DeleteOutlined} from '@ant-design/icons';
import {Link} from "react-router-dom";
import {COMMON_STATUS, NEWS_STATUS} from "../../utils/constant";
import {renderColor} from "../../utils";

function getWidth(list) {
    let total = 0
    for (const item of list) {
        total += item.width
    }
    return total
}

function TableLesson({dataSource, loading, t, pagination, onAction, onDelete, onRestore}) {

    const mappingData = (data) => {
        return data.map((item, index) => ({
            ...item,
            no: pagination.pageSize * (pagination.pageNumber - 1) + index + 1,
        }))
    }

    return (
        <Table
            dataSource={mappingData(dataSource || [])}
            columns={columns({t, onAction, onDelete, onRestore})}
            rowKey={(record) => record.id}
            loading={loading}
            pagination={pagination}
            scroll={{x: getWidth(columns({t}))}}
        />
    )
}

export default TableLesson

const columns = ({t, onAction, onDelete, onRestore}) => {
    return [
        {
            title: t("No"),
            dataIndex: "no",
            key: "no",
            fixed: "left",
            width: 70,
        },
        {
            title: t("Name(EN)"),
            dataIndex: "nameEn",
            key: "nameEn",
            fixed: "left",
            width: 120,
            render: (value, record) => (
                <div>
                    <div className="text-nowrap" style={{maxWidth: 130}}>
                        <Link
                            className="text-underline"
                            onClick={() => onAction(record)}
                        >
                            <Tooltip title={value}>{value}</Tooltip>
                        </Link>
                    </div>
                </div>
            ),
        },
        {
            title: t("Name(LC)"),
            dataIndex: "nameLc",
            key: "nameLc",
            width: 120,
        },
        {
            title: t("Content(EN)"),
            dataIndex: "contentEn",
            key: "contentEn",
            width: 200,
        },
        {
            title: t("Content(LC)"),
            dataIndex: "contentLc",
            key: "contentLc",
            width: 200,
        },
        {
            title: t("Description"),
            dataIndex: "descEn",
            key: "descEn",
            width: 150,
        },
        {
            title: t("Description(Lc)"),
            dataIndex: "descLc",
            key: "descLc",
            width: 150,
        },
        {
            title: t("Intensity"),
            dataIndex: "intensity",
            key: "intensity",
            width: 100,
        },
        {
            title: t("Level"),
            dataIndex: "level",
            key: "level",
            width: 100,
        },
        {
            title: t("Order Number"),
            dataIndex: "orderNumber",
            key: "orderNumber",
            width: 100,
        },
        {
            title: t("Status"),
            dataIndex: "status",
            key: "status",
            width: 100,
            render: (_, {status}) => status ? <Tag color="success">Active</Tag> : <Tag color="warning">Inactive</Tag>
        },
        {
            title: t("Published"),
            dataIndex: "publishedAt",
            key: "publishedAt",
            width: 100,
        },
        {
            title: "Action",
            width: 120,
            render: (value, record) => (
                <div>
                    <div className="text-nowrap" style={{maxWidth: 130}}>
                        { record.status == 1 ? 
                        <Popconfirm
                                    title="Are you sure to delete this item?"
                                    onConfirm={(e) => {
                                        onDelete(record)
                                    }}
                                    okText="Yes"
                                    cancelText="No"
                                >
                            <a href="#" style={{color: "red"}}>InActive</a>
                        </Popconfirm>
                        : <Popconfirm
                                    title="Are you sure to restore this item?"
                                    onConfirm={(e) => {
                                        onRestore(record)
                                    }}
                                    okText="Yes"
                                    cancelText="No"
                                >
                            <a href="#" style={{color: "red"}}>Active</a>
                        </Popconfirm>
                        }
                    </div>
                </div>
            ),
        },
    ]
}
