import React from "react"
import {Link} from "react-router-dom"
import {ROOT_FOLDER} from "../hooks/useFolder";
import {Breadcrumb} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {selectFolder} from "../../redux/actions";

export default function FolderBreadcrumbs({currentFolder}) {
    // let path = currentFolder === ROOT_FOLDER ? [] : [ROOT_FOLDER]
    // if (currentFolder) path = [...path, ...currentFolder.name]
    const dispatch = useDispatch()
    const {path, folder} = useSelector(state => state.common)

    const onBack = (f) => {
        console.log("llllll")
        dispatch(selectFolder({folderId: f.id, folder: f}))
    }

    return (
        <Breadcrumb
            className="flex-grow-1"
            separator={"/"}
            listProps={{className: "bg-white pl-0"}}
        >
            {[...path].map((item, index) => (
                <Breadcrumb.Item
                    key={item.id}
                    // className="text-truncate d-inline-block"
                    style={{maxWidth: "150px"}}
                >
                    {item !== folder ?<Link onClick={() => {
                        onBack(item)
                    }}>{item.name}</Link>:
                        <Link>{item.name}</Link>}
                </Breadcrumb.Item>
            ))}
            {/*{currentFolder && (*/}
            {/*    <Breadcrumb.Item*/}
            {/*        className="text-truncate d-inline-block"*/}
            {/*        style={{maxWidth: "200px"}}*/}
            {/*        active*/}
            {/*    >*/}
            {/*        {currentFolder.name}*/}
            {/*    </Breadcrumb.Item>*/}
            {/*)}*/}
        </Breadcrumb>
    )
}
