import React from "react";
import {Button, Checkbox, Form, Input, message, Modal, Select} from "antd";
import "./index.scss"
import {ROLES} from "../../utils/constant";
import {register, updateUser} from "../../redux/actions";
import {useDispatch} from "react-redux";

function ResetPassword({visible, onClose, onSuccess, id, userName }) {
    const ref = React.createRef()
    const dispatch = useDispatch()
    const [loading, setLoading] = React.useState(false)

    const onFinish = (values) => {
        if (values["password"] !== values["confirm_password"]) {
            message.error("The two passwords that you entered do not match!")
            setLoading(false)
            return false
        }
        dispatch(updateUser({
            user:{
                password: values['password'],
                userName,
                id,
            }, callback: () => {
                setLoading(false)
                onSuccess()
            }
        }))
    }

    React.useEffect(() => {
        if (ref.current)
            ref.current.setFieldsValue({
                role: ROLES[0]
            })
    }, [ref.current])

    return (
        <Modal
            visible={visible}
            onCancel={onClose}
            title={"Reset password"}
            footer={[
                <Button type={'dashed'}>Cancel</Button>,
                <Button type={'primary'} htmlType={'submit'} loading={loading} form={"update-user"}>Update</Button>
            ]}
            className="create-user is-floating-label"
        >
            <Form
                layout="vertical"
                name="basic"
                ref={ref}
                id={"update-user"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Mật khẩu mới"
                    name="password"
                    rules={[
                        {required: true, message: "Vui lòng nhập mật khẩu !"},
                    ]}
                >
                    <Input.Password/>
                </Form.Item>
                <Form.Item
                    label="Confirm password"
                    name="confirm_password"
                    rules={[
                        {
                            required: true,
                            message: "Please input your confirm password!",
                        },
                    ]}
                >
                    <Input.Password/>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default ResetPassword
