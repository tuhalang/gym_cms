import utils from "../../utils"
import {TRAINER_SUCCESS} from "../actions/action_types";

const initialState = {
    items: [],
    totalPages: 0,
    totalElements: 0
}

const trainer = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case TRAINER_SUCCESS:
            return {
                ...state,
                items: payload.items,
                totalPages: payload.totalPages,
                totalElements: payload.totalElements,
            }
        default:
            return state
    }
}

export default trainer
