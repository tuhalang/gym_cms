import Client from "../client/Client"

const resource = "/secure"

const list = (params) => {
    return Client.get(`${resource}/lesson`, {params})
}

const create = (data) => {
    return Client.post(`${resource}/lesson`, data)
}

const update = (data) => {
    return Client.put(`${resource}/lesson`, data)
}

const lessonRepository = {
    list,
    create,
    update,
}
export default lessonRepository
