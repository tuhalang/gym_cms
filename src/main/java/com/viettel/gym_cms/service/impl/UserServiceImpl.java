package com.viettel.gym_cms.service.impl;

import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.AccountDto;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.repo.UserRepo;
import com.viettel.gym_cms.security.JwtTokenProvider;
import com.viettel.gym_cms.service.UserService;
import com.viettel.gym_cms.utils.Constant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@RequiredArgsConstructor
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final UserRepo userRepo;

    @Override
    public ResponseDto signIn(AccountDto accountDTO) {

        ResponseDto responseDto = ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_NOK)
                .build();

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            accountDTO.getUsername(),
                            accountDTO.getPassword(),
                            Collections.emptyList()
                    )
            );
            SimpleGrantedAuthority sga = (SimpleGrantedAuthority) authentication.getAuthorities().iterator().next();
            String jwt = jwtTokenProvider.generateUserToken(accountDTO.getUsername(), sga.getAuthority());

            responseDto.setErrorCode(Constant.ERROR_CODE_OK);
            responseDto.setMessage(Constant.MSG_SIGN_IN_OK);
            responseDto.pushData("token", jwt);
            responseDto.pushData("roles", sga.getAuthority());
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setMessage(e.getMessage());
        }

        return responseDto;
    }

    @Override
    public ResponseDto signUp(AccountDto accountDto) {
        ResponseDto responseDto = ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_NOK)
                .build();

        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User userActive = (User) authentication.getPrincipal();

            if(!userRepo.existsByUserName(accountDto.getUsername())){
                User user = User.builder()
                        .id(userRepo.getID())
                        .userName(accountDto.getUsername())
                        .password(passwordEncoder.encode(accountDto.getPassword()))
                        .role(accountDto.getRole())
                        .status(Constant.STATUS_ACTIVE)
                        .createdBy(userActive.getUserName())
                        .createdAt(userRepo.currentDate())
                        .build();
                userRepo.save(user);

                responseDto.setErrorCode(Constant.ERROR_CODE_OK);
                responseDto.setMessage(Constant.MSG_SIGN_UP_OK);
            }else{
                responseDto.setMessage(Constant.MSG_SIGN_UP_NOK_USER_EXISTS);
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setMessage(e.getMessage());
        }

        return responseDto;
    }

    @Override
    public ResponseDto getAll(Pageable pageable, Integer status, String key, String role) {
        Page<User> page;
        if(status == null){
            if(key == null){
                key = "";
            }
            if(role == null){
                role = "";
            }
            page = userRepo.findAllByKey(pageable, key, role);
        }else{
            page = userRepo.findAllByKeyAndStatus(pageable, key, status, role);
        }

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("items", page.getContent())
                .onPushData("totalElements", page.getTotalElements())
                .onPushData("totalPages", page.getTotalPages())
                .build();
    }

    @Override
    public ResponseDto update(User user) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        User oldUser = userRepo.findByUserName(user.getUserName());

        if(oldUser != null && (oldUser.getUserName().equals(userActive.getUserName()) ||
                Constant.ROLE_ADMIN.equalsIgnoreCase(userActive.getRole()))){

            if(user.getPassword() != null && !passwordEncoder.matches(user.getPassword(), oldUser.getPassword())){
                oldUser.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            if(user.getRole() != null && !oldUser.getRole().equals(user.getRole())){
                oldUser.setRole(user.getRole());
            }
            if(user.getStatus() != null && !oldUser.getStatus().equals(user.getStatus())){
                oldUser.setStatus(user.getStatus());
            }
            userRepo.save(oldUser);

            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_OK)
                    .message(Constant.MSG_SUCCESS)
                    .build();
        }else{
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_CANNOT_UPDATE)
                    .build();
        }
    }
}
