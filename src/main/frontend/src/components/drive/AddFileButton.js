import React, {useState} from "react"
import {Button, Modal, Spin} from "antd";
import UploadFile from "../upload";
import {useDispatch, useSelector} from "react-redux";
import {createAsset, listAsset} from "../../redux/actions";

export default function AddFileButton({currentFolder}) {

    const dispatch = useDispatch()
    const [uploading, setUploading] = useState(false)
    const [loading, setLoading] = useState(false)
    const {folder} = useSelector(state => state.common)

    function handleUpload(file) {
        setLoading(true)
        dispatch(createAsset({
            file,
            name: file.name,
            type: 0,
            parentId: currentFolder ? currentFolder.id ? currentFolder.id : "" : "",
            callback: () => {
                dispatch(listAsset({
                    parentId: folder.id,
                    name: "",
                    page: 1,
                    size: 40,
                    status: 1,
                }))
                setUploading(false)
                setLoading(false)
            }
        }))
    }

    return (
        <>
            <Button type={'dashed'} onClick={() => setUploading(true)} className="btn btn-outline-success btn-sm m-0 mr-2">
                <i className='bx bxs-file-plus'></i>
            </Button>
            
                <Modal
                    centered
                    visible={uploading}
                    onOk={() => setUploading(false)}
                    onCancel={() => setUploading(false)}
                    footer={[]}>
                    <Spin tip="Uploading..." spinning={loading}>
                        <UploadFile callback={async ({files}) => {
                            setLoading(true)
                            handleUpload(files[0])
                        }}/>
                    </Spin>
                </Modal>
            
        </>
    )
}
