import React from "react";
import {AutoComplete, Button, Form, Input, message, Modal, Select, TreeSelect} from "antd";
import "./index.scss"
import {listTags, searchCategories, treeCategories} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import UploadImage from "../upload/UploadImage";

function MoreEditNews({visible, onClose, onSuccess, defaultValue}) {
    const ref = React.useRef()
    const dispatch = useDispatch()
    const category = useSelector(state => state.category)
    const [loading, setLoading] = React.useState(false)
    const [categoryInput, setCategoryInput] = React.useState({})
    const [tag, setTag] = React.useState({})
    const [value, setValue] = React.useState(null)
    const [uploading, setUploading] = React.useState(false)
    const [imageUrl, setImageUrl] = React.useState('')

    const onFinish = (values) => {
        if (values["password"] !== values["confirm_password"]) {
            message.error("The two passwords that you entered do not match!")
            setLoading(false)
            return false
        }
        onSuccess({...values, categoryId: categoryInput.id, imageUrl})
        onClose()
    }

    const onSelectCategory = (id, value) => {
        setCategoryInput({id, value})
    };

    const onSearchCategory = () => {
        dispatch(treeCategories({}))
    };

    const onSelectTag = (data, option) => {
        setTag(option.tag)
    };

    const onSearchTag = (searchText) => {
        dispatch(listTags({
            page: 1,
            size: 10,
            tagName: searchText,
        }))
    };

    React.useEffect(() => {
        dispatch(treeCategories({}))
        dispatch(listTags({
            page: 1,
            size: 10,
            tagName: ""
        }))
    }, [])

    React.useEffect(() => {
        console.log(defaultValue, "defaultValue")
        if (defaultValue && defaultValue.news && defaultValue.category) {
            ref.current.setFieldsValue({
                ...defaultValue.news,
                categoryId: defaultValue.category.nameEn,
                tags: defaultValue.newsTags.map(item => item.tagName),
            })
            setImageUrl(defaultValue.news.imageUrl)
        } else {
            ref.current.setFieldsValue({
                titleEn: "",
                titleLc: "",
                summaryEn: "",
                summaryLc: "",
                shortLink: "",
                imageUrl: "",
                videoUrl: "",
                hotNews: null,
                categoryId: null,
                tags: []
            })
        }
    }, [defaultValue])

    return (
        <div
            style={{
                display: visible ? 'flex' : 'none',
            }}
            className="create-user is-floating-label w-full flex flex-column"
        >
            <span className="title-create mb-5">Infomation of news</span>
            <Form
                layout="vertical"
                name="basic"
                ref={ref}
                id={"create-user"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Title"
                    name="titleEn"
                    rules={[{required: true, message: "Please input your title!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Title(Lc)"
                    name="titleLc"
                    rules={[{required: true, message: "Please input your title!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Summary"
                    name="summaryEn"
                    rules={[
                        {required: true, message: "Please input your summary!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Summary(Lc)"
                    name="summaryLc"
                    rules={[
                        {required: true, message: "Please input your summary!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Short Link"
                    name="shortLink"
                    rules={[
                        {
                            required: true,
                            message: "Please input your short link!",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <div className="flex">
                    <Form.Item
                        label="Image Url"
                        name="imageUrl"
                        className="flex-grow"
                        rules={[{required: false, message: "Please input your image!"}]}
                    >
                        <Input disabled/>
                    </Form.Item>
                    <Button className="m-3" onClick={() => setUploading(true)}><i
                        className='bx bxs-cloud-upload'></i></Button>
                </div>
                <Form.Item
                    label="Video Url"
                    name="videoUrl"
                    rules={[{required: false, message: "Please input your video!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Category"
                    name="categoryId"
                    rules={[
                        {
                            required: true,
                            message: "Please input your category!",
                        },
                    ]}
                >
                    <TreeSelect
                        style={{ width: '100%' }}
                        value={value}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        treeData={category.categories}
                        placeholder="Please select"
                        treeDefaultExpandAll
                        onChange={onSelectCategory}
                    />
                    {/* <AutoComplete
                        options={category.items.map(item => {
                            return {
                                id: item.id,
                                label: item.nameEn,
                                value: item.nameEn,
                                item,
                            }
                        })}
                        value={categoryInput.name || ''}
                        onSelect={onSelectCategory}
                        onSearch={onSearchCategory}
                    /> */}
                </Form.Item>
                <Form.Item
                    label="Tags"
                    name="tags"
                    rules={[
                        {
                            required: true,
                            message: "Please input your tags!",
                        },
                    ]}
                >
                    <Select
                        options={category.tags.map(tag => {
                            return {
                                id: tag.tagName,
                                label: tag.tagName,
                                value: tag.tagName,
                                tag,
                            }
                        })}
                        mode={'tags'}
                        // value={tag.tagName || ''}
                        onSelect={onSelectTag}
                        onSearch={onSearchTag}
                    />
                </Form.Item>
                <Form.Item
                    label="Hot Index"
                    name="hotNews"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    <Select>
                        <Select.Option value={1}>Yes</Select.Option>
                        <Select.Option value={0}>No</Select.Option>
                    </Select>
                </Form.Item>
                <div className="w-full flex justify-end">
                    {/*<Button type={'dashed'}>Cancel</Button>,*/}
                    <Button type={'primary'} htmlType={'submit'} loading={loading} form={"create-user"}>Next</Button>
                </div>
            </Form>
            <Modal
                title={"Upload image"}
                visible={uploading}
                onCancel={() => setUploading(false)}
                footer={[]}
            >
                <UploadImage
                    callback={async ({files}) => {
                        // handleUpload(files[0])
                        let name = files[0].name
                        let file = await toBase64(files[0])
                        setImageUrl(file)
                        ref.current.setFieldsValue({
                            imageUrl: name
                        })
                        setUploading(false)
                    }}
                />
            </Modal>
        </div>
    )
}

export default MoreEditNews

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});
