import React from "react";
import {Button, Checkbox, Form, Input, message, Modal, Select} from "antd";
import "./index.scss"
import {ROLES} from "../../utils/constant";
import {register} from "../../redux/actions";
import {useDispatch} from "react-redux";

function CreateUser({visible, onClose, onSuccess}) {
    const ref = React.createRef()
    const dispatch = useDispatch()
    const [loading, setLoading] = React.useState(false)

    const onFinish = (values) => {
        if (values["password"] !== values["confirm_password"]) {
            message.error("The two passwords that you entered do not match!")
            setLoading(false)
            return false
        }
        dispatch(register({
            ...values, callback: () => {
                setLoading(false)
                onSuccess()
            }
        }))
    }

    React.useEffect(() => {
        if (ref.current)
            ref.current.setFieldsValue({
                role: ROLES[0]
            })
    }, [ref.current])

    return (
        <Modal
            visible={visible}
            onCancel={onClose}
            destroyOnClose={true}
            maskClosable={false}
            title={"Create user"}
            footer={[
                <Button type={'dashed'}>Cancel</Button>,
                <Button type={'primary'} htmlType={'submit'} loading={loading} form={"create-user"}>Create</Button>
            ]}
            className="create-user is-floating-label"
        >
            <Form
                layout="vertical"
                name="basic"
                ref={ref}
                id={"create-user"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{required: true, message: "Vui lòng nhập username !"}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="password"
                    rules={[
                        {required: true, message: "Vui lòng nhập mật khẩu !"},
                    ]}
                >
                    <Input.Password/>
                </Form.Item>
                <Form.Item
                    label="Confirm password"
                    name="confirm_password"
                    rules={[
                        {
                            required: true,
                            message: "Please input your confirm password!",
                        },
                    ]}
                >
                    <Input.Password/>
                </Form.Item>
                <Form.Item
                    label="Role"
                    name="role"
                    rules={[
                        {
                            required: true,
                            message: "Please input your role!",
                        },
                    ]}
                >
                    <Select>
                        {ROLES.map(item => <Select.Option value={item}>{item}</Select.Option>)}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default CreateUser
