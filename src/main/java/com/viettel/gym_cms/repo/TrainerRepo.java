package com.viettel.gym_cms.repo;

import com.viettel.gym_cms.domain.Trainer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TrainerRepo extends JpaRepository<Trainer, String>, CommonRepo {

    Page<Trainer> findByNameEnContainingIgnoreCaseAndStatus(Pageable pageable, String name, Integer status);

    Page<Trainer> findByNameEnContainingIgnoreCase(Pageable pageable, String name);
}
