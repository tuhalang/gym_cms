package com.viettel.gym_cms.repo;

import com.viettel.gym_cms.domain.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo extends JpaRepository<Category, String>, CommonRepo {

    boolean existsById(String id);
    List<Category> findByParentIdAndNameEnContainingIgnoreCaseAndStatus(String parentId, String name, Integer status);
    List<Category> findByParentIdAndNameEnContainingIgnoreCase(String parentId, String name);
    List<Category> findByParentIdIsNullAndNameEnContainingIgnoreCase(String name);
    List<Category> findByParentIdIsNullAndNameEnContainingIgnoreCaseAndStatus(String name, Integer status);

    Page<Category> findByNameEnContainingIgnoreCase(Pageable pageable, String name);

    Page<Category> findByNameEnContainingIgnoreCaseAndStatus(Pageable pageable, String name, Integer status);

    @Query(
            value = "select c from Category c where c.parentId = :parentId order by c.orderNumber"
    )
    List<Category> findByParentId(String parentId);

    @Query(
            value = "select c from Category c where c.parentId is null order by c.orderNumber"
    )
    List<Category> findByParentId();
}
