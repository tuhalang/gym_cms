import React from "react";

import './index.scss'
import {Button, Col, List, message, Row, Skeleton, Popconfirm, Input} from "antd";
import {Descriptions, Space, Tag, Tree, } from "antd";
import {DeleteOutlined} from '@ant-design/icons';
import AddCategoryButton from "../../components/drive/AddCategoryButton";
import {useDispatch} from "react-redux";
import {listCategories, updateCategory} from "../../redux/actions";
import Loading from "../../components/layout/Loading";
import UpdateCategoryButton from "../../components/drive/UpdateCategoryButton";

function Category({}) {

    const dispatch = useDispatch()
    const [treeData, setTreeData] = React.useState();
    const [content, setContent] = React.useState([])
    const [meta, setMeta] = React.useState(null)
    const [loading, setLoading] = React.useState(false)

    function updateTreeData(list, key, children) {
        if (key) {
            return list.map(node => {
                if (node.key === key) {
                    return {
                        ...node,
                        children,
                    };
                }
                if (node.children) {
                    return {
                        ...node,
                        children: updateTreeData(node.children, key, children),
                    };
                }
                return node;
            });
        } else {
            console.log("test", children)
            return children;
        }
    }

    function onLoadData(node, isUpdate) {
        const {key, children, title} = node
        setMeta(node)
        return new Promise(resolve => {
            if (!isUpdate && children) {
                resolve();
                return;
            }
            setTimeout(() => {
                setLoading(true)
                dispatch(listCategories({
                    name: "",
                    page: 1,
                    size: 100,
                    parentId: key,
                    status: 1,
                    callback: () => {
                        setLoading(false)
                    },
                    callbackSuccess: (data) => {

                        setTreeData(origin =>
                            updateTreeData(origin, key, data ? data.subCategories.map(item => {
                                if (item.children) {
                                    return {
                                        ...item,
                                        title: item.nameEn,
                                        key: item.id,
                                    }
                                } else {
                                    return {
                                        ...item,
                                        title: item.nameEn,
                                        key: item.id,
                                        // isLeaf: true,
                                    }
                                }
                            }) : null),
                        );

                        if (data && data.items) setContent(data.items)
                    }
                }))
                setLoading(false);
                resolve();
            }, 1000);
        });
    }

    React.useEffect(() => {
        onLoadData({key: null, children: null})
    }, [])


    return (
        <div className="row asset" style={{
            minHeight: 600,
        }}>
            <div className="col-4 m-h-100">
                <div className="card flex flex-column m-h-100">
                    <div className="flex align-items-center w-full justify-end header-asset">
                    </div>
                    <Tree loadData={onLoadData} treeData={treeData}
                          onSelect={(selectedKeys, {selected, selectedNodes, node}) => {
                              if (selected)
                                  onLoadData(node)
                          }}/>
                </div>
            </div>
            <div className="col-8 m-h-100">
                <div className="card flex flex-column m-h-100">
                    <div className="flex items-center w-full justify-between header-asset">
                        <span>{meta && meta.nameEn}</span>
                        <Space>
                            {meta && meta.id &&
                                <Popconfirm
                                title="Are you sure to delete this item?"
                                onConfirm={(e) => {
                                    dispatch(
                                        updateCategory({
                                            category: {
                                                id: meta.id,
                                                status: 0
                                            },
                                            callback: () => {

                                            },
                                            callbackSuccess: () => {
                                                onLoadData({key: null, children: null})
                                            }
                                        })
                                    )
                                }}
                                okText="Yes"
                                cancelText="No"
                            >
                                <a href="#" ><DeleteOutlined /></a>
                            </Popconfirm>}
                            {meta && meta.id &&
                            <UpdateCategoryButton category={meta}
                                                  reLoad={() => {
                                                      setTreeData(null)
                                                      onLoadData({...meta, key: null}, true)
                                                      setMeta(undefined)
                                                  }}/>}
                            <AddCategoryButton reLoad={() => onLoadData(meta, true)} category={meta}/>
                        </Space>
                    </div>
                    {/*<Table columns={columns} dataSource={content}/>*/}
                    {loading ? <Loading/>
                        : meta && meta.id && <Descriptions bordered>
                        <Descriptions.Item label="Name(En)" span={3}>{meta.nameEn}</Descriptions.Item>
                        <Descriptions.Item label="Name(Lc)" span={3}>{meta.nameLc}</Descriptions.Item>
                        <Descriptions.Item label="Description(En)" span={3}>{meta.descEn}</Descriptions.Item>
                        <Descriptions.Item label="Description(Lc)" span={3}>{meta.descLc}</Descriptions.Item>
                        <Descriptions.Item label="Image url (En)" span={3}>
                            <a href={meta.imageUrlEn} target={'_blank'}>{meta.imageUrlEn}</a>
                        </Descriptions.Item>
                        <Descriptions.Item label="Image url (Lc)" span={3}>
                            <a href={meta.imageUrlLc} target={'_blank'}>{meta.imageUrlLc}</a>
                        </Descriptions.Item>
                        <Descriptions.Item label="Create By" span={3}>{meta.createBy || 'N/A'}</Descriptions.Item>
                        <Descriptions.Item label="Order number" span={3}>{meta.orderNumber}</Descriptions.Item>
                        <Descriptions.Item label="Trainer" span={3}>{meta.trainer && meta.trainer.nameEn}</Descriptions.Item>
                        <Descriptions.Item label="Create At" span={3}>{meta.createdAt || 'N/A'}</Descriptions.Item>
                        <Descriptions.Item label="Update At" span={3}>{meta.updatedAt || 'N/A'}</Descriptions.Item>
                        <Descriptions.Item label="Status" span={3}><Tag
                            color={'gold'}>{meta.status ? "ACTIVE" : "DEACTIVE"}</Tag></Descriptions.Item>
                    </Descriptions>}
                </div>
            </div>
        </div>
    )
}

export default Category

const columns = [
    {
        "title":
            "Name",
        "dataIndex":
            "nameEn",
        "key":
            "nameEn",
        "width":
            250
    }
    ,
    {
        "title":
            "descEn",
        "dataIndex":
            "descEn",
        "key":
            "descEn",
        "width":
            180
    }
    ,
    {
        "title":
            "descLc",
        "dataIndex":
            "descLc",
        "key":
            "descLc",
        "width":
            180
    }
    ,
    {
        "title":
            "imageUrlEn",
        "dataIndex":
            "imageUrlEn",
        "key":
            "imageUrlEn",
        "width":
            150
    }
    ,
    {
        "title":
            "imageUrlLc",
        "dataIndex":
            "imageUrlLc",
        "key":
            "imageUrlLc",
        "width":
            150
    }
    ,
    {
        "title":
            "createdBy",
        "dataIndex":
            "createdBy",
        "key":
            "createdBy",
        "width":
            150
    }
]

const options =
    {
        "expandAll":
            true
    }
