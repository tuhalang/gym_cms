package com.viettel.gym_cms.service.impl;

import com.viettel.gym_cms.domain.*;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.repo.AssetRepo;
import com.viettel.gym_cms.repo.CategoryRepo;
import com.viettel.gym_cms.repo.LessonRepo;
import com.viettel.gym_cms.repo.TrainerRepo;
import com.viettel.gym_cms.service.LessonService;
import com.viettel.gym_cms.utils.Constant;
import com.viettel.gym_cms.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class LessonServiceImpl implements LessonService {

    private final AssetRepo assetRepo;

    @Value("${app.files.path}")
    private String rootPath;

    @Value("${app.dns.path}")
    private String dnsUrl;

    private final LessonRepo lessonRepo;
    private final CategoryRepo categoryRepo;
    private final TrainerRepo trainerRepo;

    @Override
    public ResponseDto getAll(Pageable pageable, Integer status, String name, String categoryId, String trainerId) {
        Page<Lesson> page;
        if(name == null){
            name = "";
        }
        if(status == null){
            if(ObjectUtils.isEmpty(categoryId) && ObjectUtils.isEmpty(trainerId)) {
                page = lessonRepo.findByNameEnContainingIgnoreCase(pageable, name);
            } else if(ObjectUtils.isEmpty(categoryId) && !ObjectUtils.isEmpty(trainerId)){
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndTrainerId(pageable, name, trainerId);
            } else if(!ObjectUtils.isEmpty(categoryId) && ObjectUtils.isEmpty(trainerId)){
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndCategoryId(pageable, name, categoryId);
            } else {
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndTrainerIdAndCategoryId(pageable, name, trainerId, categoryId);
            }
        }else{
            if(ObjectUtils.isEmpty(categoryId) && ObjectUtils.isEmpty(trainerId)) {
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndStatus(pageable, name, status);
            } else if(ObjectUtils.isEmpty(categoryId) && !ObjectUtils.isEmpty(trainerId)){
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndStatusAndTrainerId(pageable, name, status, trainerId);
            } else if(!ObjectUtils.isEmpty(categoryId) && ObjectUtils.isEmpty(trainerId)){
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndStatusAndCategoryId(pageable, name, status, categoryId);
            } else {
                page = lessonRepo.findByNameEnContainingIgnoreCaseAndStatusAndTrainerIdAndCategoryId(pageable, name, status, trainerId, categoryId);
            }
        }

        List<Lesson> lessons = page.getContent().stream().map(l -> {
            if(l.getCategoryId() != null){
                Category category = categoryRepo.findById(l.getCategoryId()).orElse(null);
                if(category != null){
                    l.setCategoryName(category.getNameEn());
                }
            }

            if(l.getTrainerId() != null){
                Trainer trainer = trainerRepo.findById(l.getTrainerId()).orElse(null);
                if(trainer != null){
                    l.setTrainerName(trainer.getNameEn());
                }
            }
            return l;
        }).collect(Collectors.toList());

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("items", page.getContent())
                .onPushData("totalElements", page.getTotalElements())
                .onPushData("totalPages", page.getTotalPages())
                .build();
    }

    @Override
    public ResponseDto update(Lesson lesson) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        Lesson oldLesson = lessonRepo.findById(lesson.getId()).orElse(null);
        if(oldLesson != null){
            if(!ObjectUtils.isEmpty(lesson.getNameEn()) && !lesson.getNameEn().equals(oldLesson.getNameEn())){
                oldLesson.setNameEn(lesson.getNameEn());
            }

            if(!ObjectUtils.isEmpty(lesson.getNameLc()) && !lesson.getNameLc().equals(oldLesson.getNameLc())){
                oldLesson.setNameLc(lesson.getNameLc());
            }

            if(!ObjectUtils.isEmpty(lesson.getCategoryId()) && !lesson.getCategoryId().equals(oldLesson.getCategoryId())){
                oldLesson.setCategoryId(lesson.getCategoryId());
            }

            if(!ObjectUtils.isEmpty(lesson.getContentEn()) && !lesson.getContentEn().equals(oldLesson.getContentEn())){
                oldLesson.setContentEn(lesson.getContentEn());
            }

            if(!ObjectUtils.isEmpty(lesson.getContentLc()) && !lesson.getContentLc().equals(oldLesson.getContentLc())){
                oldLesson.setContentLc(lesson.getContentLc());
            }

            if(!ObjectUtils.isEmpty(lesson.getDescEn()) && !lesson.getDescEn().equals(oldLesson.getDescEn())){
                oldLesson.setDescEn(lesson.getDescEn());
            }

            if(!ObjectUtils.isEmpty(lesson.getDescLc()) && !lesson.getDescLc().equals(oldLesson.getDescLc())){
                oldLesson.setDescLc(lesson.getDescLc());
            }

            if(!ObjectUtils.isEmpty(lesson.getLevel()) && !lesson.getLevel().equals(oldLesson.getLevel())){
                oldLesson.setLevel(lesson.getLevel());
            }

            if(!ObjectUtils.isEmpty(lesson.getOrderNumber()) && !lesson.getOrderNumber().equals(oldLesson.getOrderNumber())){
                oldLesson.setOrderNumber(lesson.getOrderNumber());
            }

            if(!ObjectUtils.isEmpty(lesson.getIntensity()) && !lesson.getIntensity().equals(oldLesson.getIntensity())){
                oldLesson.setIntensity(lesson.getIntensity());
            }

            if(!ObjectUtils.isEmpty(lesson.getStatus()) && !lesson.getStatus().equals(oldLesson.getStatus())){
                oldLesson.setStatus(lesson.getStatus());
            }
            
            if(!ObjectUtils.isEmpty(lesson.getTrainerId()) && !lesson.getTrainerId().equals(oldLesson.getTrainerId())){
                oldLesson.setTrainerId(lesson.getTrainerId());
            }

            if(!ObjectUtils.isEmpty(lesson.getImageUrlEn()) && !lesson.getImageUrlEn().equals(oldLesson.getImageUrlEn())){
                String base64En = lesson.getImageUrlEn();
                String extEn = FileUtil.getExtFile(base64En);
                lesson.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));
                String imageEn = FileUtil.generateFileName(oldLesson.getId(), Constant.LANGUAGE_LC, extEn);

                try {
                    FileUtil.base64ToFile(lesson.getImageUrlEn(), rootPath + imageEn);

                    Asset assetImageLc = Asset.builder()
                            .id(assetRepo.getID())
                            .createdAt(assetRepo.currentDate())
                            .name(imageEn)
                            .link("/" + imageEn)
                            .status(Constant.STATUS_ACTIVE)
                            .type(Constant.FILE_TYPE)
                            .build();

                    assetRepo.save(assetImageLc);
                    oldLesson.setImageUrlEn(dnsUrl + "/" + imageEn);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            if(!ObjectUtils.isEmpty(lesson.getImageUrlLc()) && !lesson.getImageUrlLc().equals(oldLesson.getImageUrlLc())){
                String base64Lc = lesson.getImageUrlLc();
                String extLc = FileUtil.getExtFile(base64Lc);
                lesson.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));
                String imageLc = FileUtil.generateFileName(oldLesson.getId(), Constant.LANGUAGE_LC, extLc);

                try {
                    FileUtil.base64ToFile(lesson.getImageUrlLc(), rootPath + imageLc);

                    Asset assetImageLc = Asset.builder()
                            .id(assetRepo.getID())
                            .createdAt(assetRepo.currentDate())
                            .name(imageLc)
                            .link("/" + imageLc)
                            .status(Constant.STATUS_ACTIVE)
                            .type(Constant.FILE_TYPE)
                            .build();

                    assetRepo.save(assetImageLc);
                    oldLesson.setImageUrlLc(dnsUrl + "/" + imageLc);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            oldLesson.setUpdatedAt(lessonRepo.currentDate());
            oldLesson.setUpdatedBy(userActive.getUserName());

            lessonRepo.save(oldLesson);

        }else{
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_CANNOT_UPDATE)
                    .build();
        }
        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }

    @Override
    public ResponseDto create(Lesson lesson) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        lesson.setId(lessonRepo.getID());
        lesson.setCreatedAt(lessonRepo.currentDate());
        lesson.setCreatedBy(userActive.getUserName());
        lesson.setStatus(Constant.STATUS_ACTIVE);

        String base64En = lesson.getImageUrlEn();
        if(!ObjectUtils.isEmpty(base64En)){
            String extEn = FileUtil.getExtFile(base64En);
            lesson.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));
            String imageEn = FileUtil.generateFileName(lesson.getId(), Constant.LANGUAGE_EN, extEn);
            try {
                FileUtil.base64ToFile(lesson.getImageUrlEn(), rootPath + imageEn);

                Asset assetImageEn = Asset.builder()
                        .id(assetRepo.getID())
                        .createdAt(assetRepo.currentDate())
                        .name(imageEn)
                        .link("/" + imageEn)
                        .status(Constant.STATUS_ACTIVE)
                        .type(Constant.FILE_TYPE)
                        .build();

                assetRepo.save(assetImageEn);

                lesson.setImageUrlEn(dnsUrl + "/" + imageEn);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        String base64Lc = lesson.getImageUrlLc();
        if(!ObjectUtils.isEmpty(base64Lc)){
            String extLc = FileUtil.getExtFile(base64Lc);
            lesson.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));
            String imageLc = FileUtil.generateFileName(lesson.getId(), Constant.LANGUAGE_EN, extLc);
            try {
                FileUtil.base64ToFile(lesson.getImageUrlLc(), rootPath + imageLc);

                Asset assetImageLc = Asset.builder()
                        .id(assetRepo.getID())
                        .createdAt(assetRepo.currentDate())
                        .name(imageLc)
                        .link("/" + imageLc)
                        .status(Constant.STATUS_ACTIVE)
                        .type(Constant.FILE_TYPE)
                        .build();

                assetRepo.save(assetImageLc);

                lesson.setImageUrlLc(dnsUrl + "/" + imageLc);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }



        lessonRepo.save(lesson);

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }
}
