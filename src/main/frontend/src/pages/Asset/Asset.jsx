import React, { useState } from "react";

import './index.scss'
import useFolder from "../../components/hooks/useFolder";
import {useLocation, useParams} from "react-router-dom";
import AddFolderButton from "../../components/drive/AddFolderButton";
import AddFileButton from "../../components/drive/AddFileButton";
import FolderBreadcrumbs from "../../components/drive/FolderBreadcrumbs";
import Folder from "../../components/drive/Folder";
import File from "../../components/drive/File";
import {Button, Col, List, message, Row, Skeleton, Popconfirm, Input} from "antd";
import {DNS_IMAGE} from "../../utils/constant";
import {DeleteOutlined} from '@ant-design/icons';
import {deleteAsset, listAsset, loadMore} from "../../redux/actions";
import {useDispatch} from "react-redux";

function Asset({}) {
    const { Search } = Input;
    const {folder, childFolders, childFiles, folderId} = useFolder({})
    const dispatch = useDispatch()
    const [page, setPage] = useState(1)

    const searchFile = (name) => {
        dispatch(listAsset({
            parentId: '',
            name: name,
            page: page,
            size: 10,
            status: 1,
        }))
    }

    const deleteFile = (id) => {
        console.log(id)
        dispatch(deleteAsset({
            id,
            callback: () => {
                dispatch(listAsset({
                    parentId: folderId,
                    name: '',
                    page: page,
                    size: 10,
                    status: 1,
                }))
            }
        }))
    }

    const [loading, setLoading] = React.useState(false)

    const onLoadMore = () => {
        dispatch(loadMore({
            parentId: folderId,
            name: '',
            page: page + 1,
            size: 10,
            status: 1,
        }))
        setPage(page+1)
    };

    const loadMoreData =
        !loading ? (
            <div
                style={{
                    textAlign: 'center',
                    marginTop: 12,
                    height: 32,
                    lineHeight: '32px',
                }}
            >
                <Button onClick={onLoadMore}>loading more</Button>
            </div>
        ) : null;

    return (
        <div className="row asset">
            <div className="col-12">
                <div className="card flex flex-column">
                    <div className="flex align-items-center w-full justify-between header-asset">
                        <FolderBreadcrumbs currentFolder={folder}/>
                        <div className="flex">
                            <Search placeholder="file name" onSearch={searchFile} style={{ width: 500 }} />
                            <AddFileButton currentFolder={folder}/>
                            <AddFolderButton currentFolder={folder}/>
                        </div>
                    </div>
                    <span className="header">Folders</span>
                    {childFolders.length > 0 && (
                        <Row gutter={12} className="w-full folder-list">
                            {childFolders.map(folder => (
                                <Col
                                    key={folder.id}
                                    // className="p-2"
                                    span={6}
                                >
                                    <Folder folder={folder}/>
                                </Col>
                            ))}
                        </Row>
                    )}
                    <span className="header">Files</span>
                    {childFiles.length > 0 && (
                        <List
                            className="demo-loadmore-list p-3"
                            loading={loading}
                            itemLayout="horizontal"
                            loadMore={loadMoreData}
                            dataSource={childFiles}
                            renderItem={item => (
                                <List.Item
                                    actions={[<a key="list-loadmore-edit"
                                                 className="copy"
                                                 onClick={() => {
                                                     copyTextToClipboard(DNS_IMAGE + item.link)
                                                 }}><i
                                        className='bx bxs-copy'></i></a>,
                                        <Popconfirm
                                            title="Are you sure to delete this file?"
                                            onConfirm={(e) => {
                                                deleteFile(item.id)
                                            }}
                                            okText="Yes"
                                            cancelText="No"
                                        >
                                            <a href="#" ><DeleteOutlined /></a>
                                        </Popconfirm>,
                                    ]}
                                >
                                    <Skeleton avatar title={false} loading={item.loading} active>
                                        <File file={item}/>
                                    </Skeleton>
                                </List.Item>
                            )}
                        />
                    )}
                </div>
            </div>
        </div>
    )
}

export default Asset

function fallbackCopyTextToClipboard(text) {
    let textArea = document.createElement("textarea");
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        let successful = document.execCommand('copy');
        let msg = successful ? 'successful' : 'unsuccessful';
        message.success("Copying to clipboard was successful!")
    } catch (err) {
        message.error("Could not copy text")
    }

    document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function() {
        message.success("Copying to clipboard was successful!")
    }, function(err) {
        message.error("Could not copy text")
    });
}


