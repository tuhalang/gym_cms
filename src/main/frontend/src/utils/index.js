import {ROOT_FOLDER} from "../components/hooks/useFolder";

const getBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

export function renderColor(value) {
    const data = Number(value)
    switch (data) {
        case 1:
            return "magenta"
        case 2:
            return "#36cfc9"
        case 3:
            return "#faad14"
        case 4:
            return "#003a8c"
        case 5:
            return "#52c41a"
        case 6:
            return "#95de64"
        case 7:
            return "#237804"
        case 8:
            return "#cf1322"
        //assign color to status tag
        case 9:
            return "#250288"
        default:
            break
    }
}

const getPath = ({currPath, folder, root}) => {
    console.log("abcdefff")
    if (folder === (root || ROOT_FOLDER)) return [root || ROOT_FOLDER]
    let idx = currPath.findIndex(item => item.id === folder.id)
    if (idx > 0) {
        return currPath.slice(0, idx + 1)
    } else {
        return [...currPath, folder]
    }
}

export default {
    getBase64,
    renderColor,
    getPath,
};
