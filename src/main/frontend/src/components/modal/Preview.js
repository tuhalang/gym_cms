import React from "react";
import {Modal} from "antd";

export default function Preview({url, visible, onClose}) {
    return (
        <Modal
            visible={visible}
            onCancel={onClose}
            destroyOnClose={true}
            closable={false}
            width={550}
            footer={[]}
        >
            <video width="500" controls style={{
                borderRadius: 10,
            }}>
                <source src={url}/>
            </video>
        </Modal>
    )
}
