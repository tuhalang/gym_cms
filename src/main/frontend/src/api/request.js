import axios from "axios"
import createAuthRefreshInterceptor from "axios-auth-refresh"
import {notification} from "antd"
import {store} from "../index"
import {getMenus} from "../redux/actions/index"
import {BASE_URL} from "../utils/constant";

const http = require("http")
const https = require("https")

function getAccessToken() {
    return localStorage.getItem("accessToken")
}

function getRefreshToken() {
    return localStorage.getItem("refreshToken")
}

const refreshAuthLogic = (failedRequest) =>
    axios
        .post(`${BASE_URL}auth/refresh`, {
            refresh_token: getRefreshToken(),
        })
        .then((tokenRefreshResponse) => {
            localStorage.setItem("accessToken", tokenRefreshResponse.data.token)

            failedRequest.response.config.headers["Authorization"] =
                "Bearer " + tokenRefreshResponse.data.token
            if (failedRequest.response.config.url === "/permissions/menus") {
                store.dispatch(getMenus())
            }
            return Promise.resolve()
        })
        .catch(() => {
            localStorage.clear()
            axios.delete("/auth/register", {token: getRefreshToken()})
            // window.location.href = "/login"
        })

export default function getInstanceAxios(baseAPI) {
    const instance = axios.create({
        baseURL: baseAPI,
        httpAgent: new http.Agent({keepAlive: true}),
        httpsAgent: new https.Agent({keepAlive: true}),
    })

    createAuthRefreshInterceptor(instance, refreshAuthLogic)

    instance.interceptors.request.use(
        function (config) {
            config.headers = {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: getAccessToken()
                    ? `Bearer ${getAccessToken()}`
                    : undefined,
                // "Accept-language": localStorage.getItem("locale")
                //     ? localStorage.getItem("locale")
                //     : "vi",
            }
            return config
        },
        function (error) {
            return Promise.reject(error)
        }
    )

    instance.interceptors.response.use(
        function (response) {
            try {
                if (response.status >= 200 && response.status < 300)
                    return response.data
                return Promise.reject(response.data)
            } catch (error) {
                return Promise.reject(error)
            }
        },
        async function (error) {
            if (error.response) {
                if (error.response.status === 401 || error.response.status === 403) {
                    localStorage.clear()
                    window.open("/")
                    return Promise.reject(error)
                }
                const {response} = error
                const data = response.data
                if (data.message && response.config.method !== "get") {
                    if (data.details && data.details.length > 0) {
                        notification.error({
                            message: data.details[0].msg,
                        })
                    } else {
                        notification.error({
                            message: data.message,
                        })
                    }
                }
            }
            return Promise.reject(error)
        }
    )

    createAuthRefreshInterceptor(instance, refreshAuthLogic)
    return instance
}
