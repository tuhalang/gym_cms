import {
    LIST_ASSET_SUCCESS,
    SELECT_FOLDER,
    LIST_USERS_SUCCESS,
    UPDATE_FOLDER,
    LOAD_MORE_SUCCEED
} from "../actions/action_types";
import utils from "../../utils"
import {ROOT_FOLDER} from "../../components/hooks/useFolder";

const initialState = {
    childFolders: [],
    childFiles: [],
    folderId: null,
    folder: ROOT_FOLDER,
    path: [ROOT_FOLDER],
    items: [],
    totalPages: 0,
    totalElements: 0
}

const common = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case SELECT_FOLDER:
            return {
                ...state,
                folderId: payload.folderId || null,
                folder: payload.folder || null,
                childFolders: [],
                childFiles: [],
                path: utils.getPath({currPath: state.path, folder: payload.folder})
            }
        case UPDATE_FOLDER:
            return {
                ...state,
                folder: payload.folder
            }
        case LIST_ASSET_SUCCESS:
            return {
                ...state,
                folderId: payload.parentId,
                childFolders: payload.subFolder,
                childFiles: payload.items
            }
        case LIST_USERS_SUCCESS:
            return {
                ...state,
                items: payload.items,
                totalPages: payload.totalPages,
                totalElements: payload.totalElements
            }
        case LOAD_MORE_SUCCEED:
            console.log(state.childFiles)
            console.log(payload.items)
            return {
                ...state,
                childFiles: state.childFiles.concat(payload.items),
                totalPages: payload.totalPages,
                totalElements: payload.totalElements
            }
        default:
            return state
    }
}

export default common
