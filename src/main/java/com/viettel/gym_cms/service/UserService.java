package com.viettel.gym_cms.service;

import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.AccountDto;
import com.viettel.gym_cms.dto.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface UserService {

    ResponseDto signIn(AccountDto accountDto);
    ResponseDto signUp(AccountDto accountDto);

    ResponseDto getAll(Pageable pageable, Integer status, String key, String role);

    ResponseDto update(User user);
}
