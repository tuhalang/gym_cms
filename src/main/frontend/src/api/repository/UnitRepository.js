import Client from "../client/Client"

const resource = "/secure"

const list = (params) => {
    return Client.get(`${resource}/unit`, {params})
}

const create = (data) => {
    return Client.post(`${resource}/unit`, data)
}

const update = (data) => {
    return Client.put(`${resource}/unit`, data)
}

const unitRepository = {
    list,
    create,
    update,
}
export default unitRepository
