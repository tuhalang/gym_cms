import React from "react";
import {useDispatch, useSelector} from "react-redux";
import Filter from "./Filter";
import {paramsUrl} from "../../components/function";
import {withTranslation} from "react-i18next";
import {trainer, unit, updateTrainer} from "../../redux/actions";
import EditUnit from "../../components/modal/EditUnit";
import TableTrainer from "./TableTrainer";
import EditTrainer from "../../components/modal/EditTrainer";

function Unit({t}) {
    const dispatch = useDispatch()
    const themeReducer = useSelector(state => state.ThemeReducer)
    const {items, totalPages, totalElements} = useSelector(state => state.trainer)
    const [loading, setLoading] = React.useState(false)
    const [valueEditing, setValueEditing] = React.useState(undefined)
    const [editing, setEditing] = React.useState(false)

    const [__pagination, __setPagination] = React.useState({
        pageNumber: 1,
        pageSize: 10,
        total: 0,
        status: "-1",
        name: "",
        ...paramsUrl.get(),
    })

    const fetch = ({}) => {
        setLoading(true)
        switch (__pagination.status) {
            case "-1": //all
                dispatch(trainer({
                    ...__pagination,
                    page: __pagination.pageNumber,
                    size: __pagination.pageSize,
                    status: null,
                    callback: () => {
                        setLoading(false)
                    }
                }))
                break;
            default:
                dispatch(trainer({
                    ...__pagination,
                    page: __pagination.pageNumber,
                    size: __pagination.pageSize,
                    callback: () => {
                        setLoading(false)
                    }
                }))
                break;
        }
    }

    function onFilter(params) {
        __setPagination({
            ...__pagination,
            ...params,
        })
    }

    async function changePage(pageNumber, pageSize) {
        __setPagination({...__pagination, pageNumber, pageSize})
    }

    React.useEffect(() => {
        fetch({})
    }, [__pagination])

    return (
        <div>
            <h2 className={`page-header ${themeReducer.word}`}>
                trainer
            </h2>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <Filter
                            onFilter={onFilter}
                            onFetch={() => fetch({})}
                            filter={__pagination}
                        />
                        <TableTrainer
                            dataSource={items}
                            loading={loading}
                            onAction={(record) => {
                                setValueEditing(record)
                                setTimeout(() =>
                                    setEditing(true), 10)
                            }}
                            onDelete={(record) => {
                                dispatch(updateTrainer({
                                    id: record.id,
                                    status: 0,
                                    callback: () => {
                                        fetch({})
                                    }
                                }))
                            }}
                            onRestore={(record) => {
                                dispatch(updateTrainer({
                                    id: record.id,
                                    status: 1,
                                    callback: () => {
                                        fetch({})
                                    }
                                }))
                            }}
                            pagination={{...__pagination, onChange: changePage, total: totalElements, type: 'large'}}
                            t={t}
                        />
                    </div>
                </div>
            </div>
            <EditTrainer
                visible={editing}
                onClose={() => {
                    setEditing(false)
                    fetch({})
                }}
                defaultValue={valueEditing}
            />
        </div>
    )
}

export default withTranslation()(Unit)
