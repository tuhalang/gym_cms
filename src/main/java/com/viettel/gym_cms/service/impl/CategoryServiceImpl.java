package com.viettel.gym_cms.service.impl;

import com.viettel.gym_cms.domain.Asset;
import com.viettel.gym_cms.domain.Category;
import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.repo.AssetRepo;
import com.viettel.gym_cms.repo.CategoryRepo;
import com.viettel.gym_cms.service.CategoryService;
import com.viettel.gym_cms.utils.Constant;
import com.viettel.gym_cms.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepo categoryRepo;
    private final AssetRepo assetRepo;

    @Value("${app.files.path}")
    private String rootPath;

    @Value("${app.dns.path}")
    private String dnsUrl;


    @Override
    public ResponseDto create(Category category) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        String parentId = null;

        if (!ObjectUtils.isEmpty(category.getParentId())) {
            if (categoryRepo.existsById(category.getParentId())) {
                parentId = category.getParentId();
            } else {
                return ResponseDto.builder()
                        .errorCode(Constant.ERROR_CODE_NOK)
                        .message(Constant.MSG_PARENT_ID_NOT_EXISTS)
                        .build();
            }
        }

        String base64En = category.getImageUrlEn();
        String base64Lc = category.getImageUrlLc();

        String extEn = FileUtil.getExtFile(base64En);
        String extLc = FileUtil.getExtFile(base64Lc);

        category.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));
        category.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));

        String categoryId = categoryRepo.getID();

        String imageEn = FileUtil.generateFileName(categoryId, Constant.LANGUAGE_EN, extEn);
        String imageLc = FileUtil.generateFileName(categoryId, Constant.LANGUAGE_LC, extLc);

        try {
            FileUtil.base64ToFile(category.getImageUrlEn(), rootPath + imageEn);
            FileUtil.base64ToFile(category.getImageUrlLc(), rootPath + imageLc);

            Asset assetImageEn = Asset.builder()
                    .id(assetRepo.getID())
                    .createdAt(assetRepo.currentDate())
                    .name(imageEn)
                    .link("/" + imageEn)
                    .status(Constant.STATUS_ACTIVE)
                    .type(Constant.FILE_TYPE)
                    .build();

            Asset assetImageLc = Asset.builder()
                    .id(assetRepo.getID())
                    .createdAt(assetRepo.currentDate())
                    .name(imageLc)
                    .link("/" + imageLc)
                    .status(Constant.STATUS_ACTIVE)
                    .type(Constant.FILE_TYPE)
                    .build();

            assetRepo.save(assetImageEn);
            assetRepo.save(assetImageLc);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        category.setImageUrlEn(dnsUrl + "/" + imageEn);
        category.setImageUrlLc(dnsUrl + "/" + imageLc);

        Category c = Category.builder()
                .id(categoryId)
                .parentId(parentId)
                .nameEn(category.getNameEn())
                .nameLc(category.getNameLc())
                .descEn(category.getDescEn())
                .descLc(category.getDescLc())
                .orderNumber(category.getOrderNumber())
                .imageUrlEn(category.getImageUrlEn())
                .imageUrlLc(category.getImageUrlLc())
                .status(Constant.STATUS_ACTIVE)
                .createdAt(categoryRepo.currentDate())
                .createdBy(userActive.getUserName())
                .build();
        categoryRepo.save(c);

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }

    @Override
    public ResponseDto update(Category category) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        Category oldCategory = categoryRepo.findById(category.getId()).orElse(null);

        if(oldCategory != null){
            if(!ObjectUtils.isEmpty(category.getParentId()) && !category.getParentId().equals(oldCategory.getParentId())){
                oldCategory.setParentId(category.getParentId());
            }
            if(!ObjectUtils.isEmpty(category.getNameEn()) && !category.getNameEn().equals(oldCategory.getNameEn())){
                oldCategory.setNameEn(category.getNameEn());
            }
            if(!ObjectUtils.isEmpty(category.getNameLc()) && !category.getNameLc().equals(oldCategory.getNameLc())){
                oldCategory.setNameLc(category.getNameLc());
            }
            if(!ObjectUtils.isEmpty(category.getDescEn()) && !category.getDescEn().equals(oldCategory.getDescEn())){
                oldCategory.setDescEn(category.getDescEn());
            }
            if(!ObjectUtils.isEmpty(category.getDescLc()) && !category.getDescLc().equals(oldCategory.getDescLc())){
                oldCategory.setDescLc(category.getDescLc());
            }
            if(!ObjectUtils.isEmpty(category.getImageUrlEn()) && !category.getImageUrlEn().equals(oldCategory.getImageUrlEn())){
                String base64En = category.getImageUrlEn();
                String extEn = FileUtil.getExtFile(base64En);
                category.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));
                String imageEn = FileUtil.generateFileName(oldCategory.getId(), Constant.LANGUAGE_LC, extEn);

                try {
                    FileUtil.base64ToFile(category.getImageUrlEn(), rootPath + imageEn);

                    Asset assetImageLc = Asset.builder()
                            .id(assetRepo.getID())
                            .createdAt(assetRepo.currentDate())
                            .name(imageEn)
                            .link("/" + imageEn)
                            .status(Constant.STATUS_ACTIVE)
                            .type(Constant.FILE_TYPE)
                            .build();

                    assetRepo.save(assetImageLc);
                    oldCategory.setImageUrlEn(dnsUrl + "/" + imageEn);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if(!ObjectUtils.isEmpty(category.getImageUrlLc()) && !category.getImageUrlLc().equals(oldCategory.getImageUrlLc())){
                String base64Lc = category.getImageUrlLc();
                String extLc = FileUtil.getExtFile(base64Lc);
                category.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));
                String imageLc = FileUtil.generateFileName(oldCategory.getId(), Constant.LANGUAGE_LC, extLc);

                try {
                    FileUtil.base64ToFile(category.getImageUrlLc(), rootPath + imageLc);

                    Asset assetImageLc = Asset.builder()
                            .id(assetRepo.getID())
                            .createdAt(assetRepo.currentDate())
                            .name(imageLc)
                            .link("/" + imageLc)
                            .status(Constant.STATUS_ACTIVE)
                            .type(Constant.FILE_TYPE)
                            .build();

                    assetRepo.save(assetImageLc);
                    oldCategory.setImageUrlLc(dnsUrl + "/" + imageLc);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if(!ObjectUtils.isEmpty(category.getStatus()) && !category.getStatus().equals(oldCategory.getStatus())){
                oldCategory.setStatus(category.getStatus());
            }
            if(!ObjectUtils.isEmpty(category.getOrderNumber()) && !category.getOrderNumber().equals(oldCategory.getOrderNumber())){
                oldCategory.setOrderNumber(category.getOrderNumber());
            }

            oldCategory.setUpdatedAt(categoryRepo.currentDate());
            oldCategory.setUpdatedBy(userActive.getUserName());

            categoryRepo.save(oldCategory);
        } else{
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_CANNOT_UPDATE)
                    .build();
        }
        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }

    @Override
    public ResponseDto retrieve(Pageable pageable, String parentId, String name, Integer status) {

        List<Category> subCategories = null;


        if(name == null){
            name = "";
        }

        if(status == null){
            if(parentId == null || "".equals(parentId)){
                subCategories = categoryRepo.findByParentIdIsNullAndNameEnContainingIgnoreCase(name);
            }else {
                subCategories = categoryRepo.findByParentIdAndNameEnContainingIgnoreCase(parentId, name);
            }
        }else{
            if(parentId == null || "".equals(parentId)){
                subCategories = categoryRepo.findByParentIdIsNullAndNameEnContainingIgnoreCaseAndStatus(name, status);
            }else {
                subCategories = categoryRepo.findByParentIdAndNameEnContainingIgnoreCaseAndStatus(parentId, name, status);
            }
        }


        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("parentId", parentId)
                .onPushData("subCategories", subCategories)
                .build();
    }

    @Override
    public ResponseDto search(Pageable pageable, String name, Integer status) {
        if(name == null){
            name = "";
        }

        Page<Category> page = null;
        if(status == null){
            page = categoryRepo.findByNameEnContainingIgnoreCase(pageable, name);
        }else{
            page = categoryRepo.findByNameEnContainingIgnoreCaseAndStatus(pageable, name, status);
        }

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("totalElements", page.getTotalElements())
                .onPushData("totalPages", page.getTotalPages())
                .onPushData("items", page.getContent())
                .build();
    }

    @Override
    public ResponseDto getTree(String parentId) {
        List<Category> categories;
        if(ObjectUtils.isEmpty(parentId)){
            categories = getSub();
        }else{
            categories = getSub(parentId);
        }

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("categories", categories)
                .build();
    }

    public List<Category> getSub(String parentId){
        return categoryRepo.findByParentId(parentId).stream().map(c ->{
            c.setChildren(getSub(c.getId()));
            c.setKey(c.getId());
            c.setTitle(c.getNameEn());
            c.setValue(c.getId());
            return c;
        }).collect(Collectors.toList());
    }

    public List<Category> getSub(){
        return categoryRepo.findByParentId().stream().map(c ->{
            c.setChildren(getSub(c.getId()));
            c.setKey(c.getId());
            c.setTitle(c.getNameEn());
            c.setValue(c.getId());
            return c;
        }).collect(Collectors.toList());
    }
}
