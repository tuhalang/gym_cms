package com.viettel.gym_cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymsCmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GymsCmsApplication.class, args);
    }

}
