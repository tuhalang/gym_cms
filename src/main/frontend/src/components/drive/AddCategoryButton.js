import React, {useState} from "react"
import {ROOT_FOLDER} from "../hooks/useFolder";
import {Button, Form, Input, Modal} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {createAsset, listAsset} from "../../redux/actions";
import EditCategory from "../modal/EditCategory";

export default function AddCategoryButton({category, reLoad}) {
    const [open, setOpen] = useState(false)

    function closeModal() {
        setOpen(false)
    }

    function openModal() {
        setOpen(true)
    }

    return (
        <>
            <Button type={'dashed'} onClick={openModal} className="btn btn-outline-success btn-sm">
                <i className='bx bxs-duplicate'></i>
            </Button>
            <EditCategory
                visible={open}
                reLoad={reLoad}
                create={true}
                onClose={closeModal}
                category={category}
            />
        </>
    )
}
