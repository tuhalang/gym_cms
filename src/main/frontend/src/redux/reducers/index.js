import {combineReducers} from "redux"
import ThemeReducer from "./ThemeReducer"
import user from "./user"
import common from  "./common"
import category from "./category"
import lesson from "./lesson"
import unit from "./unit"
import trainer from "./trainer"

const rootReducer = combineReducers({
    user,
    common,
    category,
    ThemeReducer,
    lesson,
    unit,
    trainer,
})

export default rootReducer
