package com.viettel.gym_cms.service;

import com.viettel.gym_cms.domain.Unit;
import com.viettel.gym_cms.dto.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface UnitService {

    ResponseDto getAll(Pageable pageable, Integer status, String name, String lessonId);

    ResponseDto update(Unit unit);

    ResponseDto create(Unit unit);
}
