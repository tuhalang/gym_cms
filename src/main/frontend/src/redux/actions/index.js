import {
    CREATE_ASSET,
    DELETE_ASSET,
    GET_MENUS,
    LIST_ASSET,
    LIST_ASSET_SUCCESS,
    LOGIN,
    LOGIN_SUCCESS,
    REGISTER,
    REGISTER_SUCCESS,
    SELECT_FOLDER,
    LIST_USERS,
    LIST_USERS_SUCCESS,
    NEWS_APPROVE,
    SAVE_DRAFT,
    UPDATE_DRAFT,
    NEWS_EXPIRE,
    NEWS_PUBLISH,
    NEWS_RETRIEVE,
    NEWS_SEARCH,
    NEWS_SUBMIT,
    UPDATE_NEWS,
    ALL_NEWS,
    ALL_NEWS_SUCCESS,
    LIST_CATEGORIES,
    CREATE_CATEGORIES,
    UPDATE_CATEGORIES,
    LIST_CATEGORIES_SUCCESS,
    LIST_TAGS,
    LIST_TAGS_SUCCESS,
    UPDATE_USER,
    SEARCH_CATEGORIES,
    SEARCH_CATEGORIES_SUCCESS,
    NEWS_RETRIEVE_SUCCESS,
    RESET_EDIT_NEWS,
    SELECT_FOLDER_CATEGORY,
    UPDATE_CATEGORY_LOCAL,
    TREE_CATEGORIES,
    TREE_CATEGORIES_SUCCESS,
    NEWS_SEARCH_SUCCESS,
    LESSON,
    CREATE_LESSON,
    UPDATE_LESSON,
    UPDATE_TRAINER,
    CREATE_TRAINER, TRAINER, UPDATE_UNIT, CREATE_UNIT, UNIT, TRAINER_SUCCESS, UNIT_SUCCESS, LESSON_SUCCESS, LOAD_MORE
} from "./action_types";

export const getMenus = () => {
    return {
        type: GET_MENUS
    }
}

export const login = ({username, password}) => {
    return {
        type: LOGIN,
        params: {
            username,
            password,
        }
    }
}

export const loginSuccess = () => {
    return {
        type: LOGIN_SUCCESS
    }
}

export const register = ({username, password, role, callback}) => {
    return {
        type: REGISTER,
        params: {
            username,
            password,
            role,
        },
        callback,
    }
}

export const registerSuccess = () => {
    return {
        type: REGISTER_SUCCESS
    }
}

export const listUsersSuccess = ({payload}) => {
    return {
        type: LIST_USERS_SUCCESS,
        payload
    }
}

export const listUsers = ({page, size, role, key}) => {
    return {
        type: LIST_USERS,
        params: {
            page,
            size,
            role,
            key
        }
    }
}


export const listAsset = ({parentId, name, page, size, status}) => {
    return {
        type: LIST_ASSET,
        params: {
            parentId,
            name,
            page,
            size,
            status,
        }
    }
}

export const listAssetSuccess = ({payload}) => {
    return {
        type: LIST_ASSET_SUCCESS,
        payload
    }
}

export const createAsset = ({file, name, parentId, type, callback}) => {
    return {
        type: CREATE_ASSET,
        params: {
            file,
            name,
            parentId,
            type,
        },
        callback,
    }
}

export const deleteAsset = ({id, callback}) => {
    return {
        type: DELETE_ASSET,
        params: {
            id
        },
        callback,
    }
} 


export const selectFolder = ({folderId, folder}) => {
    return {
        type: SELECT_FOLDER,
        payload: {
            folderId,
            folder,
        }
    }
}

export const approveNews = ({newsId, status, callback}) => {
    return {
        type: NEWS_APPROVE,
        payload: {
            newsId,
            status,
        },
        callback,
    }
}

export const saveNewsDraft = ({news, tags, callback}) => {
    return {
        type: SAVE_DRAFT,
        payload: {
            news,
            tags,
        },
        callback,
    }
}

export const updateNewsDraft = ({news, tags, callback}) => {
    return {
        type: UPDATE_DRAFT,
        payload: {
            news,
            tags,
        },
        callback,
    }
}

export const expireNews = ({newsId, callback}) => {
    return {
        type: NEWS_EXPIRE,
        payload: {newsId},
        callback,
    }
}

export const publishNews = ({newsId, callback}) => {
    return {
        type: NEWS_PUBLISH,
        payload: {newsId},
        callback,
    }
}

export const getNewsRetrieve = ({newsId, shortLink, callback}) => {
    return {
        type: NEWS_RETRIEVE,
        payload: {
            newsId,
            shortLink,
        },
        callback,
    }
}

export const getNewsRetrieveSuccess = ({payload}) => {
    return {
        type: NEWS_RETRIEVE_SUCCESS,
        payload,
    }
}

export const searchNews = ({categoryId, page, size, tagName, title, fromDate, toDate, status, callback}) => {
    return {
        type: NEWS_SEARCH,
        payload: {
            categoryId,
            page,
            size,
            tagName,
            title,
            fromDate,
            toDate,
            status,
        },
        callback,
    }
}

export const searchNewsSuccess = ({payload}) => {
    return {
        type: NEWS_SEARCH_SUCCESS,
        payload,
    }
}

export const submitNews = ({newsId, callback}) => {
    return {
        type: NEWS_SUBMIT,
        payload: {
            newsId,
        },
        callback,
    }
}

export const updateNews = ({news, tags, callback}) => {
    return {
        type: UPDATE_NEWS,
        payload: {
            news,
            tags,
        },
        callback,
    }
}

export const allNews = ({page, size, status, title, callback}) => {
    return {
        type: ALL_NEWS,
        payload: {
            page,
            size,
            status,
            title,
        },
        callback,
    }
}

export const allNewsSuccess = ({payload}) => {
    return {
        type: ALL_NEWS_SUCCESS,
        payload,
    }
}

export const listCategories = ({name, page, size, parentId, status, callback, callbackSuccess}) => {
    return {
        type: LIST_CATEGORIES,
        payload: {
            name,
            page,
            size,
            parentId,
            status,
        },
        callback,
        callbackSuccess,
    }
}

export const listCategoriesSuccess = ({payload}) => {
    return {
        type: LIST_CATEGORIES_SUCCESS,
        payload,
    }
}

export const createCategory = ({category, callback, callbackSuccess}) => {
    return {
        type: CREATE_CATEGORIES,
        payload: {...category},
        callback,
        callbackSuccess,
    }
}

export const updateCategory = ({category, callback, callbackSuccess}) => {
    return {
        type: UPDATE_CATEGORIES,
        payload: {...category},
        callback,
        callbackSuccess,
    }
}

export const listTags = ({page, size, tagName}) => {
    return {
        type: LIST_TAGS,
        payload: {
            page,
            size,
            tagName
        }
    }
}

export const listTagsSuccess = ({payload}) => {
    return {
        type: LIST_TAGS_SUCCESS,
        payload,
    }
}

export const updateUser = ({user, callback}) => {
    return {
        type: UPDATE_USER,
        payload: {
            ...user,
        },
        callback,
    }
}

export const searchCategories = ({name, page, size, status}) => {
    return {
        type: SEARCH_CATEGORIES,
        payload: {
            name,
            page,
            size,
            status,
        }
    }
}

export const searchCategoriesSuccess = ({payload}) => {
    return {
        type: SEARCH_CATEGORIES_SUCCESS,
        payload,
    }
}

export const resetEditNews = () => {
    return {
        type: RESET_EDIT_NEWS
    }
}

export const selectFolderCategory = ({folderId, folder}) => {
    return {
        type: SELECT_FOLDER_CATEGORY,
        payload: {
            folderId,
            folder,
        }
    }
}

export const updateCategoryLocal = ({folder}) => {
    return {
        type: UPDATE_CATEGORY_LOCAL,
        payload: {folder},
    }
}

export const treeCategories = ({parentId}) => {
    return {
        type: TREE_CATEGORIES,
        payload: {
            parentId,
        }
    }
}

export const treeCategoriesSuccess = (data) => {
    return {
        type: TREE_CATEGORIES_SUCCESS,
        payload: {...data}
    }
}

export const lesson = ({name, page, size, status, trainerId, categoryId, callback}) => {
    return {
        type: LESSON,
        payload: {
            name,
            page,
            size,
            status,
            trainerId,
            categoryId,
        },
        callback,
    }
}

export const lessonSuccess = (data) => {
    return {
        type: LESSON_SUCCESS,
        payload: {...data}
    }
}

export const createLesson = ({
                                 categoryId,
                                 contentEn,
                                 contentLc,
                                 descEn,
                                 descLc,
                                 imageUrlEn,
                                 imageUrlLc,
                                 intensity,
                                 level,
                                 nameEn,
                                 nameLc,
                                 orderNumber,
                                 status,
                                 trainerId,
                                 callback,
                             }) => {
    return {
        type: CREATE_LESSON,
        payload: {
            categoryId,
            contentEn,
            contentLc,
            descEn,
            descLc,
            imageUrlEn,
            imageUrlLc,
            intensity,
            level,
            nameEn,
            nameLc,
            orderNumber,
            status,
            trainerId,
        },
        callback,
    }
}

export const updateLesson = ({
                                 categoryId,
                                 contentEn,
                                 contentLc,
                                 descEn,
                                 descLc,
                                 imageUrlEn,
                                 imageUrlLc,
                                 intensity,
                                 level,
                                 nameEn,
                                 nameLc,
                                 orderNumber,
                                 status,
                                 trainerId,
                                 id,
                                 callback,
                             }) => {
    return {
        type: UPDATE_LESSON,
        payload: {
            categoryId,
            contentEn,
            contentLc,
            descEn,
            descLc,
            imageUrlEn,
            imageUrlLc,
            intensity,
            level,
            nameEn,
            nameLc,
            orderNumber,
            status,
            trainerId,
            id,
        },
        callback,
    }
}

export const unit = ({name, page, size, status, lessonId, callback}) => {
    return {
        type: UNIT,
        payload: {
            name,
            page,
            size,
            status,
            lessonId,
        },
        callback,
    }
}

export const unitSuccess = (data) => {
    return {
        type: UNIT_SUCCESS,
        payload: {...data}
    }
}

export const createUnit = ({
                               contentEn,
                               contentLc,
                               estimateTime,
                               lessonId,
                               videoUrlEn,
                               videoUrlLc,
                               nameEn,
                               nameLc,
                               orderNumber,
                               status,
                               callback,
                           }) => {
    return {
        type: CREATE_UNIT,
        payload: {
            estimateTime,
            contentEn,
            contentLc,
            lessonId,
            videoUrlEn,
            videoUrlLc,
            nameEn,
            nameLc,
            orderNumber,
            status,
        },
        callback,
    }
}

export const updateUnit = ({
                               contentEn,
                               contentLc,
                               estimateTime,
                               lessonId,
                               videoUrlEn,
                               videoUrlLc,
                               nameEn,
                               nameLc,
                               orderNumber,
                               status,
                               id,
                               callback,
                           }) => {
    return {
        type: UPDATE_UNIT,
        payload: {
            contentEn,
            contentLc,
            estimateTime,
            lessonId,
            videoUrlEn,
            videoUrlLc,
            nameEn,
            nameLc,
            orderNumber,
            status,
            id,
        },
        callback,
    }
}

export const trainer = ({name, page, size, status, callback}) => {
    return {
        type: TRAINER,
        payload: {
            name,
            page,
            size,
            status,
        },
        callback,
    }
}

export const trainerSuccess = (data) => {
    return {
        type: TRAINER_SUCCESS,
        payload: {...data}
    }
}

export const createTrainer = ({
                                  descEn,
                                  descLc,
                                  imageUrl,
                                  nameEn,
                                  nameLc,
                                  status,
                                  callback,
                              }) => {
    return {
        type: CREATE_TRAINER,
        payload: {
            imageUrl,
            descEn,
            descLc,
            nameEn,
            nameLc,
            status,
        },
        callback,
    }
}

export const updateTrainer = ({
                                  descEn,
                                  descLc,
                                  imageUrl,
                                  nameEn,
                                  nameLc,
                                  status,
                                  id,
                                  callback,
                              }) => {
    return {
        type: UPDATE_TRAINER,
        payload: {
            descEn,
            descLc,
            imageUrl,
            nameEn,
            nameLc,
            status,
            id,
        },
        callback,
    }
}

export const loadMore = ({parentId, name, page, size, status}) => {
    return {
        type: LOAD_MORE,
        params: {
            parentId,
            name,
            page,
            size,
            status,
        }
    }
}