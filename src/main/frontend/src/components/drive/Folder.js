import React from "react"
import {Link} from "react-router-dom"
import './index.scss'
import {useDispatch} from "react-redux";
import {selectFolder} from "../../redux/actions";

export default function Folder({folder}) {
    const dispatch = useDispatch()

    return (
        <Link
            // to={{
            //     pathname: `/asset/${folder.id}`,
            //     state: { folder: folder }
            // }}
            onClick={() => dispatch(selectFolder({folderId: folder.id, folder}))}
            className="btn btn-outline-dark text-truncate w-100"
            className="folder flex items-center"
        >
            <i className='bx bxs-folder icon'></i>
            <span className="name">{folder.name}</span>
        </Link>
    )
}
