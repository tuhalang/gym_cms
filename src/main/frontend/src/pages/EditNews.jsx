import React from "react";

import '../assets/css/editor.scss'
import {Editor} from "@tinymce/tinymce-react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, message, Select, Space, Steps} from "antd";
import MoreEditNews from "../components/modal/MoreEditNews";
import {getNewsRetrieve, resetEditNews, saveNewsDraft, updateNews, updateNewsDraft} from "../redux/actions";
import {useHistory, useParams} from "react-router-dom";

const plugins = "print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons"
const toolbar = "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl"
const menubar = "file edit view insert format tools table help"
const imagetools_cors_hosts = ['picsum.photos']

//example: https://codepen.io/tinymce/pen/dyGPYqw

function EditNews({}) {

    const dispatch = useDispatch()
    const themeReducer = useSelector(state => state.ThemeReducer)
    const {post} = useSelector(state => state.news)
    const [editing, setEditing] = React.useState(true)
    const {id} = useParams();
    const [info, setInfo] = React.useState({})
    const [loading, setLoading] = React.useState(false)
    const [contentEn, setContentEn] = React.useState('')
    const [contentLc, setContentLc] = React.useState('')
    const [editLang, setEditLang] = React.useState('English')
    const [step, setStep] = React.useState(0)
    const roles = localStorage.getItem('roles')
    const history = useHistory()
    const [current, setCurrent] = React.useState(0);

    const next = () => {
        setCurrent(current + 1);
    };

    const prev = () => {
        setCurrent(current - 1);
    };

    const handleEditorChange = (e) => {
        if (editLang === "English")
            setContentEn(e.target.getContent())
        else setContentLc(e.target.getContent())
    }

    function handleSubmit() {
        if (!contentEn || contentEn === '') {
            message.error('No content English!')
            return
        }
        if (!contentLc || contentLc === '') {
            message.error('No content Portuguese!')
            return
        }
        setLoading(true)
        if (id && id != '') {
            if (post.news && post.news.status < 3) {
                dispatch(updateNewsDraft({
                    news: {
                        ...info,
                        contentEn,
                        contentLc,
                        id,
                    },
                    tags: info.tags.map(item => ({
                        tagName: item,
                        description: "",
                    })),
                    callback: () => {
                        setLoading(false)
                        history.push('/news')
                    }
                }))
            } else {
                dispatch(updateNews({
                    news: {
                        ...info,
                        contentEn,
                        contentLc,
                        id,
                    },
                    tags: info.tags.map(item => ({
                        tagName: item,
                        description: "",
                    })),
                    callback: () => {
                        setLoading(false)
                        history.push('/news')
                    }
                }))
            }
        } else {
            dispatch(saveNewsDraft({
                news: {...info, contentEn, contentLc},
                tags: info.tags.map(item => ({
                    tagName: item,
                    description: "",
                })),
                callback: () => {
                    setLoading(false)
                    history.push('/news')
                }
            }))
        }
    }

    const steps = [
        {
            title: 'Edit Info',
            content: <MoreEditNews
                visible={editing}
                defaultValue={post}
                onClose={() => setEditing(false)}
                onSuccess={(values) => {
                    setInfo(values)
                    setEditing(false)
                    next()
                }}
            />,
        },
        {
            title: 'Edit Content',
            content: <>
                <div className="editor" style={{
                    display: editing ? 'none' : "block"
                }}>
                    <div style={{
                        display: step === 0 ? "block" : "none"
                    }}>
                        <Editor
                            id={"english"}
                            apiKey="0mexo29p3o6n23gbxx4ud6vpoeau5wm3hdxe7l6y0w4mivmh"
                            scriptLoading={{async: true}}
                            onInit={(evt, editor) => console.log(evt)}
                            initialValue={post && post.news ? post.news.contentEn : ""}
                            // value={contentEn}
                            init={{
                                height: 500,
                                menubar: menubar,
                                plugins: [plugins],
                                toolbar: toolbar
                            }}
                            onChange={handleEditorChange}
                        />
                    </div>
                    <div style={{
                        display: step === 0 ? "none" : "block"
                    }}>
                        <Editor
                            id={"portuguese"}
                            apiKey="0mexo29p3o6n23gbxx4ud6vpoeau5wm3hdxe7l6y0w4mivmh"
                            scriptLoading={{async: true}}
                            onInit={(evt, editor) => console.log(evt)}
                            initialValue={post && post.news ? post.news.contentLc : ""}
                            // value={contentEn}
                            init={{
                                height: 500,
                                menubar: menubar,
                                plugins: [plugins],
                                toolbar: toolbar
                            }}
                            onChange={handleEditorChange}
                        />
                    </div>
                    <div className="flex justify-between mt-4">
                        <Space>
                            <Select value={editLang} onChange={(value) => {
                                if (value === "English") setStep(0)
                                else setStep(1)
                                setEditLang(value)
                            }}
                                    style={{width: 150}}>
                                <Select.Option value={'English'}>English</Select.Option>
                                <Select.Option value={'Portuguese'}>Portuguese</Select.Option>
                            </Select>
                            <Alert message="Please select all language" type="warning" showIcon closable/>
                        </Space>
                        <Space>
                            <Button type={'dashed'} onClick={() => {
                                setEditing(true)
                                prev()
                            }}>Previous</Button>
                            <Button type={'primary'} onClick={handleSubmit} loading={loading}
                                    disabled={roles !== "ADMIN" && post.news && post.news.status > 2}>Submit</Button>
                        </Space>
                    </div>
                </div>
            </>,
        },
    ];

    React.useEffect(() => {
        if (id && id != '')
            dispatch(getNewsRetrieve({
                newsId: id,
                shortLink: "",
                callback: () => {
                }
            }))
        else {
            dispatch(resetEditNews())
        }
    }, [id])

    React.useEffect(() => {
        if (id && id != '' && post && post.news) {
            setContentLc(post.news.contentLc)
            setContentEn(post.news.contentEn)
        }
    }, [post])

    return (
        <div>
            <h2 className={`page-header ${themeReducer.word}`}>
                Edit News
            </h2>
            <Steps current={current} className="m-4" style={{
                width: 300,
            }}>
                {steps.map(item => (
                    <Steps.Step key={item.title} title={item.title}/>
                ))}
            </Steps>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        {steps[current].content}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EditNews
