package com.viettel.gym_cms.repo;

import com.viettel.gym_cms.domain.Lesson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonRepo extends JpaRepository<Lesson, String>, CommonRepo {

    Page<Lesson> findByNameEnContainingIgnoreCaseAndStatus(Pageable pageable, String name, Integer status);

    Page<Lesson> findByNameEnContainingIgnoreCaseAndStatusAndCategoryId(Pageable pageable, String name, Integer status, String categoryId);

    Page<Lesson> findByNameEnContainingIgnoreCaseAndStatusAndTrainerId(Pageable pageable, String name, Integer status, String trainerId);

    Page<Lesson> findByNameEnContainingIgnoreCaseAndStatusAndTrainerIdAndCategoryId(Pageable pageable, String name, Integer status, String trainerId, String categoryId);

    Page<Lesson> findByNameEnContainingIgnoreCase(Pageable pageable, String name);

    Page<Lesson> findByNameEnContainingIgnoreCaseAndCategoryId(Pageable pageable, String name, String categoryId);

    Page<Lesson> findByNameEnContainingIgnoreCaseAndTrainerId(Pageable pageable, String name, String trainerId);

    Page<Lesson> findByNameEnContainingIgnoreCaseAndTrainerIdAndCategoryId(Pageable pageable, String name, String trainerId, String categoryId);

}
