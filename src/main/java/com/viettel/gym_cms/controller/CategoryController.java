package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.domain.Category;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class CategoryController {

    private final CategoryService categoryService;

    @RequestMapping(value = "/categories", method = RequestMethod.POST)
    public ResponseDto create(@RequestBody Category category){
        return categoryService.create(category);
    }

    @RequestMapping(value = "/categories/tree", method = RequestMethod.GET)
    public ResponseDto getTree(@RequestParam(required = false) String parentId){
        return categoryService.getTree(parentId);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.PUT)
    public ResponseDto update(@RequestBody Category category){
        return categoryService.update(category);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public ResponseDto retrieve(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(value = "parentId", required = false) String parentId,
                                @RequestParam(value = "name", required = false) String name,
                                @RequestParam(value = "status", required = false) Integer status){
        Pageable pageable = PageRequest.of(page-1, size);
        return categoryService.retrieve(pageable, parentId, name, status);
    }

    @RequestMapping(value = "/categories/search", method = RequestMethod.GET)
    public ResponseDto search(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(value = "name", required = false) String name,
                                @RequestParam(value = "status", required = false) Integer status){
        Pageable pageable = PageRequest.of(page-1, size);
        return categoryService.search(pageable, name, status);
    }
}
