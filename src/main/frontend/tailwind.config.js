module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    backgroundColor: (theme) => ({
      ...theme("colors"),
      primary: "#E10000",
      secondary: "#002672",
      danger: "#e3342f",
      title: "#BFBFBF",
    }),
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
