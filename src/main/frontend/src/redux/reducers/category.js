import {
    LIST_CATEGORIES_SUCCESS,
    LIST_TAGS_SUCCESS,
    SEARCH_CATEGORIES_SUCCESS,
    SELECT_FOLDER_CATEGORY, TREE_CATEGORIES_SUCCESS, UPDATE_CATEGORY_LOCAL,
    UPDATE_FOLDER_CATEGORY
} from "../actions/action_types";
import utils from "../../utils"

import {ROOT_FOLDER_CATEGORY} from "../../components/hooks/useCategory";

const initialState = {
    items: [],
    categories: [],
    totalPages: 0,
    totalElements: 0,
    tags: [],
    childFolders: [],
    childFiles: [],
    folderId: null,
    folder: ROOT_FOLDER_CATEGORY,
    path: [ROOT_FOLDER_CATEGORY],
    tree: null,
}

const common = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case LIST_CATEGORIES_SUCCESS:
            return {
                ...state,
                subCategories: payload.subCategories,
                items: payload.items,
                totalPages: payload.totalPages,
                totalElements: payload.totalElements,
                folderId: payload.parentId,
                childFolders: payload.subCategories,
                childFiles: payload.items
            }
        case SEARCH_CATEGORIES_SUCCESS:
            console.log("search", payload.items)
            return {
                ...state,
                items: payload.items,
                totalPages: payload.totalPages,
                totalElements: payload.totalElements,
            }
        case TREE_CATEGORIES_SUCCESS:
            console.log("tree", payload.items)
            return {
                ...state,
                categories: payload.categories,
            }
        case LIST_TAGS_SUCCESS:
            console.log(payload.items, "payload")
            return {
                ...state,
                tags: payload.items || [],
            }
        case SELECT_FOLDER_CATEGORY:
            return {
                ...state,
                folderId: payload.folderId || null,
                folder: payload.folder || null,
                childFolders: [],
                childFiles: [],
                path: utils.getPath({currPath: state.path, folder: payload.folder, root: ROOT_FOLDER_CATEGORY})
            }
        case UPDATE_FOLDER_CATEGORY:
            return {
                ...state,
                folder: payload.folder
            }
        case UPDATE_CATEGORY_LOCAL:
            return {
                ...state,
                folder: {
                    ...state.folder,
                    ...payload.folder
                }
            }
        case TREE_CATEGORIES_SUCCESS:
            return {
                ...state,
                tree: payload.categories,
            }
        default:
            return state
    }
}

export default common
