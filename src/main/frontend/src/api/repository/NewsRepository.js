import Client from "../client/Client"

const resource = "/secure/news"

const approve = (params) => {
    return Client.get(`${resource}/approve`, {params})
}

const draft = (data) => {
    return Client.post(`${resource}/draft`, data)
}

const updateDraft = (data) => {
    return Client.put(`${resource}/draft`, data)
}

const expire = (params) => {
    return Client.get(`${resource}/expire`, {params})
}

const publish = (params) => {
    return Client.get(`${resource}/publish`, {params})
}

const retrieve = (params) => {
    return Client.get(`${resource}/retrieve`, {params})
}

const search = (params) => {
    return Client.get(`${resource}/search`, {params})
}

const submit = (params) => {
    return Client.get(`${resource}/submit`, {params})
}

const update = (params) => {
    return Client.put(`${resource}/update`, params)
}

const list = (params) => {
    return Client.get(`${resource}/getAll`, {params})
}

const newsRepository = {
    approve,
    draft,
    updateDraft,
    expire,
    publish,
    retrieve,
    search,
    submit,
    update,
    list,
}
export default newsRepository
