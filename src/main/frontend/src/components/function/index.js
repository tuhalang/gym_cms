import queryString from "query-string"
import moment from "moment"

export function selectedDate() {
    return {
        "Pass week": [
            moment().subtract(1, "weeks").startOf("week"),
            moment().subtract(1, "weeks").endOf("week"),
        ],
        "Pass month": [moment().startOf("month"), moment().endOf("month")],
        "Pass 3 months": [
            moment().subtract(3, "months").startOf("month"),
            moment().endOf("month"),
        ],
        "Pass 6 months": [
            moment().subtract(6, "months").startOf("month"),
            moment(),
        ],
        "Pass year": [moment().subtract(1, "year").startOf("year"), moment()],
    }
}

export function renderColor(value) {
    const data = Number(value)
    switch (data) {
        case 1:
            return "magenta"
        case 2:
            return "#36cfc9"
        case 3:
            return "#faad14"
        case 4:
            return "#003a8c"
        case 5:
            return "#52c41a"
        case 6:
            return "#95de64"
        case 7:
            return "#237804"
        case 8:
            return "#cf1322"
        default:
            break
    }
}

export const paramsUrl = {
    get: () => {
        return queryString.parse(window.location.search)
    },
    set: (params) => {
        const currentUrlParams = queryString.stringify(params, {
            skipNull: true,
            skipEmptyString: true,
        })
        window.history.pushState(
            {},
            null,
            `${window.location.pathname}?${currentUrlParams.toString()}`
        )
    },
}

export function renderColorRefund(key) {
    let item = {
        name: "Refund request",
        color: "",
    }
    if (key === 1) {
        item = {
            name: "Refund processed",
            color: "#108ee9",
        }
    }
    if (key === 2) {
        item = {
            name: "Refund done",
            color: "#87d068",
        }
    }
    return item
}
