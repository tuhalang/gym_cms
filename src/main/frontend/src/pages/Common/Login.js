import React, {useState} from "react"
import {Checkbox, Form, Input, Button, Row, Col} from "antd"
import {Link} from "react-router-dom"
import "./index.scss"
import wordwideBg from "../../assets/images/wordwide.svg"
import {login} from "../../redux/actions";
import {useDispatch} from "react-redux";
import {PRIMARY_COLOR} from "../../utils/constant";

const Login = () => {
    // const history = useHistory()
    const dispatch = useDispatch()
    // const onChangeUserInfo = (value) => dispatch(setUserInfo(value))
    const [loading, setLoading] = useState(false)
    const r = localStorage.getItem("r")
    const _name = localStorage.getItem("n")

    function handleSubmit(values) {
        dispatch(login({...values}))
    }

    function clearRegisterData() {
        localStorage.setItem("r", "")
    }

    function setUser() {
        let values = {}
        values = {
            username: "news_admin",
            password: "123456a@",
        }
        handleSubmit(values)
    }

    function onFinishFailed(params) {
    }

    return (
        <Row className="page-container vcs-page">
            <Col xs={24} sm={12} md={12} lg={10}>
                <div className="logo flex items-end">
                    <div className="logo-vcs"/>
                    <div className="logo-text ml-2">Mov Gym System</div>
                </div>
                <div className="page_login">
                    <div className="login_form is-floating-label">
                        <div className="text4xl f-500 mb-4">SignIn</div>
                        {r === "s" ? (
                            <p> Hello {_name}, Let login to use system ! </p>
                        ) : (
                            <p></p>
                        )}
                        <Form
                            layout="vertical"
                            name="basic"
                            initialValues={{remember: true}}
                            onFinish={handleSubmit}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Username"
                                name="username"
                                rules={[{required: true, message: "Please input your username !"}]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[
                                    {required: true, message: "Please input your password !"},
                                ]}
                            >
                                <Input.Password/>
                            </Form.Item>
                            {/* <Form.Item>
                                <Form.Item name="remember" valuePropName="checked" noStyle>
                                    <Checkbox>Ghi nhớ </Checkbox>
                                </Form.Item>

                                <Link className="login-form-forgot" to="/forgot-password">
                                    Quên mật khẩu
                                </Link>
                            </Form.Item> */}
                            <Form.Item>
                                <Button
                                    className="w-right vcs-button w-200"
                                    type="primary"
                                    htmlType="submit"
                                    loading={loading}
                                >
                                    SignIn
                                </Button>
                            </Form.Item>
                            {/* {process.env.NODE_ENV === "development" && (
                                <Row>
                                    <Col>
                                        <Button type="text" onClick={() => setUser()}>
                                            Login as Development
                                        </Button>
                                    </Col>
                                </Row>
                            )} */}
                        </Form>
                    </div>
                </div>
            </Col>
            <Col xs={0} sm={12} md={12} lg={14}>
                <div className="page_info">
                    <img className="page_img" alt="" src={wordwideBg}/>
                    <div className="container">
                        <h1>Mov Gym System</h1>
                        {/* <p>
                            Sport news là trang cung cấp tin tức về thể thao từ mọi nơi trên thế giới nhanh chóng chuẩn xác đến cho mọi người
                        </p> */}
                    </div>
                </div>
            </Col>
        </Row>
    )
}

export default Login
