import {GET_MENUS_SUCCESS, LOGIN_SUCCESS, REGISTER_SUCCESS} from "../actions/action_types";

const dfUser = {
    avatar:
        "http://res.cloudinary.com/desslrvfx/image/upload/v1601693396/eager-balder/upload/kqbpsnen2uhicndtgxus.jpg",
    email: "",
    id: undefined,
    joined_at: "",
    merchant_id: undefined,
    merchant_type: "",
    name: "Test",
    team: "",
}

const initialState = {
    user: dfUser,
    menus: [],
    permissions: [],
    loading: false,
    balanceUser: {
        balance: 0,
        currency: "USD",
    },
}

const user = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }

    switch (action.type) {
        case GET_MENUS_SUCCESS:
            return {...state, menus: action.payload}
        case LOGIN_SUCCESS:
            window.open('/GYM_CMS', '_self')
        default:
            return state
    }
}

export default user
