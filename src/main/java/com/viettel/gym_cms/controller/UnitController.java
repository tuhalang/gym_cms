package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.domain.Unit;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.UnitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class UnitController {

    private final UnitService unitService;

    @RequestMapping(value = "/unit", method = RequestMethod.POST)
    ResponseDto create(@RequestBody Unit unit){
        return unitService.create(unit);
    }

    @RequestMapping(value = "/unit", method = RequestMethod.PUT)
    ResponseDto update(@RequestBody Unit unit){
        return unitService.update(unit);
    }

    @RequestMapping(value = "/unit", method = RequestMethod.GET)
    ResponseDto getAll(@RequestParam Integer page,
                       @RequestParam Integer size,
                       @RequestParam(required = false) String name,
                       @RequestParam(required = false) String lessonId,
                       @RequestParam(required = false) Integer status){
        Pageable pageable = PageRequest.of(page-1, size, Sort.by("orderNumber").ascending());
        return unitService.getAll(pageable, status, name, lessonId);
    }
}
