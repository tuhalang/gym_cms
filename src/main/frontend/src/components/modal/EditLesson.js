import React from "react";
import {AutoComplete, Button, Form, Input, message, Modal, Select, TreeSelect} from "antd";
import "./index.scss"
import {createLesson, listTags, searchCategories, trainer, treeCategories, updateLesson} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import UploadImage from "../upload/UploadImage";
import {ClearOutlined} from "@ant-design/icons";

function EditLesson({visible, onClose, defaultValue, key}) {
    const ref = React.useRef()
    const dispatch = useDispatch()
    const category = useSelector(state => state.category)
    const trainerState = useSelector(state => state.trainer)
    const [loading, setLoading] = React.useState(false)
    const [categoryInput, setCategoryInput] = React.useState({})
    const [trainerInput, setTrainerInput] = React.useState({})
    const [tag, setTag] = React.useState({})
    const [value, setValue] = React.useState(null)
    const [uploading, setUploading] = React.useState(null)
    const [imageUrlEn, setImageUrlEn] = React.useState('')
    const [imageUrlLc, setImageUrlLc] = React.useState('')

    const onFinish = (values) => {
        setLoading(true)
        if (defaultValue && defaultValue.id !== "") {
            dispatch(updateLesson({
                ...values,
                id: defaultValue.id,
                imageUrlLc,
                imageUrlEn,
                categoryId: categoryInput.id,
                trainerId: trainerInput.id,
                callback: () => {
                    console.log("CALL_BACK", onClose)
                    onClose()
                    setLoading(false)
                }
            }))
        } else {
            dispatch(createLesson({
                ...values,
                imageUrlLc,
                imageUrlEn,
                categoryId: categoryInput.id,
                trainerId: trainerInput.id,
                callback: () => {
                    setLoading(false)
                    onClose()
                }
            }))
        }
    }

    const onSelectCategory = (_, {id, value}) => {
        setCategoryInput({id, value})
    };

    const onSelectTrainer = (_, {id, value}) => {
        setTrainerInput({id, value})
    }

    const onFetchCategories = (data) => {
        dispatch(searchCategories({
            name: data,
            page: 1,
            size: 10,
            status: 1,
        }))
    };

    const onFetchTrainer = (data) => {
        dispatch(trainer({
            name: data,
            page: 1,
            size: 10,
            status: 1,
        }))
    }

    React.useEffect(() => {
        console.log("key: " + key)
        if (defaultValue && defaultValue.id !== "") {
            console.log(defaultValue, "defaultValue")
            if (ref.current)
                ref.current.setFieldsValue({...defaultValue})
            else setTimeout(() => ref.current.setFieldsValue({...defaultValue}), 50)
            setImageUrlEn(defaultValue.imageUrlEn)
            setImageUrlLc(defaultValue.imageUrlLc)
        }else{
            console.log("Clear cache")
            defaultValue = {}
            setImageUrlEn("")
            setImageUrlLc("")
            setCategoryInput({})
            setTrainerInput({})
        }
        console.log("default value: " + defaultValue)
    }, [defaultValue])

    return (
        <Modal
            visible={visible}
            width={900}
            onCancel={onClose}
            maskClosable={false}
            destroyOnClose={true}
            footer={[<Button type={'primary'} htmlType={'submit'} loading={loading}
                             form={"create-user"}>Submit</Button>]}
            className="create-user is-floating-label w-full flex flex-column"
        >
            <span className="title-create mb-5">{!(defaultValue && defaultValue.id !== "") ? "Create a new lesson" : "Update lesson"}</span>
            <br/>
            <br/>
            <Form
                layout="vertical"
                name="basic"
                ref={ref}
                id={"create-user"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Name"
                    name="nameEn"
                    rules={[{required: true, message: "Please input your name!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Name(Lc)"
                    name="nameLc"
                    rules={[{required: true, message: "Please input your name!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Content"
                    name="contentEn"
                    rules={[
                        {required: true, message: "Please input your content!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Content(Lc)"
                    name="contentLc"
                    rules={[
                        {required: true, message: "Please input your content!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Description"
                    name="descEn"
                    rules={[
                        {
                            required: true,
                            message: "Please input your desc!",
                        },
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Description(Lc)"
                    name="descLc"
                    className="flex-grow"
                    rules={[{required: false, message: "Please input your desc!"}]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <div className="flex">
                    <Form.Item
                        label="Image"
                        name="imageUrlEn"
                        className="flex-grow"
                        rules={[{required: false, message: "Please input your image!"}]}
                    >
                        <Input disabled/>
                    </Form.Item>
                    <Button className="m-3" onClick={() => setUploading('imageUrlEn')}><i
                        className='bx bxs-cloud-upload'></i></Button>
                </div>
                <div className="flex">
                    <Form.Item
                        label="Image(Lc)"
                        name="imageUrlLc"
                        className="flex-grow"
                        rules={[{required: false, message: "Please input your image!"}]}
                    >
                        <Input disabled/>
                    </Form.Item>
                    <Button className="m-3" onClick={() => setUploading('imageUrlLc')}><i
                        className='bx bxs-cloud-upload'></i></Button>
                </div>
                <Form.Item
                    label="Intensity"
                    name="intensity"
                    rules={[
                        {
                            required: true,
                            message: "Please input your intensity!",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Level"
                    name="level"
                    rules={[
                        {
                            required: true,
                            message: "Please input your level!",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Order Number"
                    name="orderNumber"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Trainer"
                    name="trainerName"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    <AutoComplete
                        className="w-full"
                        options={trainerState.items.map(item => ({
                            label: item.nameEn,
                            value: item.nameEn,
                            id: item.id,
                        }))}
                        onSelect={onSelectTrainer}
                        onSearch={onFetchTrainer}
                        placeholder={'--Trainer--'}
                    />
                </Form.Item>
                <Form.Item
                    label="Category"
                    name="categoryName"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    {category.items && <AutoComplete
                        className="w-full"
                        options={category.items.map(item => ({
                            label: item.nameEn,
                            value: item.nameEn,
                            id: item.id,
                        }))}
                        onSelect={onSelectCategory}
                        onSearch={onFetchCategories}
                        placeholder={'--Category--'}
                    />}
                </Form.Item>
            </Form>
            <Modal
                title={"Upload image"}
                visible={uploading}
                destroyOnClose={true}
                onCancel={() => setUploading(null)}
                footer={[]}
            >
                <UploadImage
                    callback={async ({files}) => {
                        // handleUpload(files[0])
                        let name = files[0].name
                        let file = await toBase64(files[0])
                        if (uploading === "imageUrlEn") {
                            setImageUrlEn(file)
                        } else setImageUrlLc(file)
                        ref.current.setFieldsValue({
                            [uploading]: name
                        })
                        setUploading(null)
                    }}
                />
            </Modal>
        </Modal>
    )
}

export default EditLesson

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});
