package com.viettel.gym_cms.service.impl;

import com.viettel.gym_cms.domain.Asset;
import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.repo.AssetRepo;
import com.viettel.gym_cms.service.AssetService;
import com.viettel.gym_cms.utils.Constant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class AssetServiceImpl implements AssetService {

    @Value("${app.files.path}")
    private String rootPath;

    @Value("${app.dns.path}")
    private String dnsUrl;

    private final AssetRepo assetRepo;

    @Override
    public ResponseDto upload(String parentId, Integer type, String name, MultipartFile file) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        String parentPath = "";

        if(!ObjectUtils.isEmpty(parentId)){
            Asset asset = assetRepo.findById(parentId).orElse(null);
            if(asset == null){
                return ResponseDto.builder()
                        .errorCode(Constant.ERROR_CODE_NOK)
                        .message(Constant.MSG_PARENT_ID_NOT_EXISTS)
                        .build();
            }else{
                parentPath += asset.getLink();
            }
        }

        if(Constant.FILE_TYPE.equals(type)){
            if (!file.isEmpty()) {
                try {
                    String filePath =  parentPath + "/" + file.getOriginalFilename().replaceAll("\\s+","");
                    File dest = new File(rootPath + filePath);
                    if(dest.exists()){
                        filePath = parentPath + "/" + System.currentTimeMillis() + file.getOriginalFilename().replaceAll("\\s+","");
                    }
                    dest = new File(rootPath + filePath);
                    file.transferTo(dest);

                    Asset asset = Asset.builder()
                            .id(assetRepo.getID())
                            .name(file.getOriginalFilename().replaceAll("\\s+",""))
                            .parentId(parentId)
                            .link(filePath)
                            .type(Constant.FILE_TYPE)
                            .status(Constant.STATUS_ACTIVE)
                            .createdAt(assetRepo.currentDate())
                            .createdBy(userActive.getUserName())
                            .build();
                    assetRepo.save(asset);
                }catch (Exception e){
                    log.error(e.getMessage(), e);
                    return ResponseDto.builder()
                            .errorCode(Constant.ERROR_CODE_NOK)
                            .message(e.getMessage())
                            .build();
                }
            } else {
                return ResponseDto.builder()
                        .errorCode(Constant.ERROR_CODE_NOK)
                        .message(Constant.MSG_FILE_EMPTY)
                        .build();
            }
        }else{

            String id = assetRepo.getID();
            String filePath = parentPath + "/" + id;
            File folder = new File(rootPath + filePath);
            folder.mkdirs();

            Asset asset = Asset.builder()
                    .id(id)
                    .name(name.replaceAll("\\s+",""))
                    .parentId(parentId)
                    .link(filePath)
                    .type(Constant.FOLDER_TYPE)
                    .status(Constant.STATUS_ACTIVE)
                    .createdAt(assetRepo.currentDate())
                    .createdBy(userActive.getUserName())
                    .build();
            assetRepo.save(asset);
        }
        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }

    @Override
    public ResponseDto retrieve(Pageable pageable, String parentId, String name, Integer status) {
        List<Asset> subFolder;
        Page<Asset> page;
        if(ObjectUtils.isEmpty(name)){
            name = "";
            if(status == null){
                if(ObjectUtils.isEmpty(parentId)){
                    subFolder = assetRepo.findByParentIdIsNullAndTypeOrderByCreatedAtDesc(Constant.FOLDER_TYPE);
                    page = assetRepo.findByParentIdIsNullAndNameContainingIgnoreCaseAndTypeOrderByCreatedAtDesc(pageable, name, Constant.FILE_TYPE);
                }else {
                    subFolder = assetRepo.findByParentIdAndTypeOrderByCreatedAtDesc(parentId, Constant.FOLDER_TYPE);
                    page = assetRepo.findByParentIdAndNameContainingIgnoreCaseAndTypeOrderByCreatedAtDesc(pageable, parentId, name, Constant.FILE_TYPE);
                }
            }else{
                if(ObjectUtils.isEmpty(parentId)){
                    subFolder = assetRepo.findByParentIdIsNullAndTypeAndStatusOrderByCreatedAtDesc(Constant.FOLDER_TYPE, status);
                    page = assetRepo.findByParentIdIsNullAndNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(pageable, name, status, Constant.FILE_TYPE);
                }else {
                    subFolder = assetRepo.findByParentIdAndTypeAndStatusOrderByCreatedAtDesc(parentId, Constant.FOLDER_TYPE, status);
                    page = assetRepo.findByParentIdAndNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(pageable, parentId, name, status, Constant.FILE_TYPE);
                }
            }
        }else{
            subFolder = assetRepo.findByNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(name, Constant.STATUS_ACTIVE, Constant.FOLDER_TYPE);
            page = assetRepo.findByNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(pageable, name, Constant.STATUS_ACTIVE, Constant.FILE_TYPE);
        }

        List<Asset> files = page.getContent().stream().map(e -> {
            e.setLink(dnsUrl + e.getLink());
            return e;
        }).collect(Collectors.toList());


        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("parentId", parentId)
                .onPushData("subFolder", subFolder)
                .onPushData("items", files)
                .onPushData("totalElements", page.getTotalElements())
                .onPushData("totalPages", page.getTotalPages())
                .build();
    }

    @Override
    public ResponseDto delete(String id) {

        Asset asset = assetRepo.findById(id).orElse(null);
        if(asset == null){
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_SUCCESS)
                    .build();
        }

        String pathFile = rootPath + asset.getLink();
        File file = new File(pathFile);
        if(file.exists()){
            file.delete();
        }

        assetRepo.delete(asset);

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }
}
