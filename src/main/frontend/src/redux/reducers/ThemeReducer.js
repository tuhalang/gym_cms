const theme = {
    light: "theme-mode-light",
    dark: "theme-mode-dark",
}

const word = {
    light: "word-color-light",
    dark: "word-color-dark",
}

const result = {
    light: "result-404-light",
    dark: "result-404-dark",
}

const ThemeReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_MODE':
            if (action.payload === theme.dark) {
                return {
                    ...state,
                    mode: theme.dark,
                    result: result.dark,
                    word: word.dark
                }
            } else {
                return {
                    ...state,
                    mode: theme.light,
                    result: result.light,
                    word: word.light
                }
            }
        case 'SET_COLOR':
            return {
                ...state,
                color: action.payload
            }
        default:
            return state
    }
}

export default ThemeReducer
