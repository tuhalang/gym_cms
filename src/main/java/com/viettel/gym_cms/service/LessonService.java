package com.viettel.gym_cms.service;

import com.viettel.gym_cms.domain.Lesson;
import com.viettel.gym_cms.dto.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface LessonService {

    ResponseDto getAll(Pageable pageable, Integer status, String name, String categoryId, String trainerId);

    ResponseDto update(Lesson lesson);

    ResponseDto create(Lesson lesson);
}
