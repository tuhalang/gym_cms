package com.viettel.gym_cms.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "trainer")
public class Trainer {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "name_en")
    private String nameEn;
    @Column(name = "name_lc")
    private String nameLc;
    @Column(name = "desc_en")
    private String descEn;
    @Column(name = "desc_lc")
    private String descLc;
    @Column(name = "image_url")
    private String imageUrl;
    @Column(name = "status")
    private Integer status;
    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_at")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "updated_by")
    private String updatedBy;
}
