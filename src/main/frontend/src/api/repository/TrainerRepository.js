import Client from "../client/Client"

const resource = "/secure"

const list = (params) => {
    return Client.get(`${resource}/trainer`, {params})
}

const create = (data) => {
    return Client.post(`${resource}/trainer`, data)
}

const update = (data) => {
    return Client.put(`${resource}/trainer`, data)
}

const trainerRepository = {
    list,
    create,
    update,
}
export default trainerRepository
