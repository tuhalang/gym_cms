import React, {useRef} from "react"
import {
    Row,
    Col,
    Input,
    Tabs,
    DatePicker, Space, Button, Select, AutoComplete, Tooltip,
} from "antd"
import {ClearOutlined, SearchOutlined, UserAddOutlined} from "@ant-design/icons"
import moment from "moment"
import {withTranslation} from "react-i18next"
import {paramsUrl, selectedDate} from "../../components/function";
import {NEWS_STATUS, USER_STATUS} from "../../utils/constant";
import {useDispatch, useSelector} from "react-redux";
import {approveNews, expireNews, publishNews, searchCategories, submitNews} from "../../redux/actions";
import {Link, useHistory} from "react-router-dom";
import CreateUser from "../../components/modal/CreateUser";

const {TabPane} = Tabs
const dfColFilter = {sm: 12, md: 12, lg: 8, xl: 8}

const Filter = ({onFilter, filter, isAll, selectedRowKeys, t, handleOk, role, onFetch}) => {
    const dispatch = useDispatch()
    const category = useSelector(state => state.category)
    const history = useHistory()
    const [creating, setCreating] = React.useState(false)
    const __filter = useRef({
        ...paramsUrl.get()
    })

    function onChangeFilter(key, value) {
        if (key === "date") {
            __filter.current.fromDate = value[0] || undefined
            __filter.current.toDate = value[1] || undefined
        } else if (key === "role") {
            __filter.current.role = value
        } else {
            __filter.current[key] = value
        }
        onFilter(__filter.current)
    }


    const {fromDate, toDate, search} = filter

    const onSelect = (data, {id}) => {
        onChangeFilter("categoryId", id)
    };
    const onSearch = (data) => {
        dispatch(searchCategories({
            name: data,
            page: 1,
            size: 10,
            status: 1,
        }))
    };

    React.useEffect(() => {
        onSearch("")
    }, [])

    const {items} = category

    return (
        <div className="items-center m-4 card-filter">
            <Row>
                {isAll && (
                    <Tabs
                        className="w-full"
                        defaultActiveKey={filter ? filter.role : "1"}
                        onChange={(value) =>
                            onChangeFilter("role", value)
                        }
                    >
                        {USER_STATUS.length > 0 &&
                        USER_STATUS.map(({id, code}) => {
                            return (
                                <TabPane
                                    tab={<span className="capitalize">{code}</span>}
                                    key={id.toString()}
                                />
                            )
                        })}
                    </Tabs>
                )}
            </Row>
            <div className="flex justify-end">
                <Space className="items-start">
                    <div className="flex justify-end w-full">
                        <Button icon={<UserAddOutlined/>} type="primary" style={{marginBottom: 16}}
                                className="mr-5"
                                onClick={() => setCreating(true)}>Create</Button>
                    </div>
                </Space>
            </div>
            <CreateUser
                visible={creating}
                onClose={() => setCreating(false)}
                onSuccess={() => {
                    handleOk()
                    setCreating(false)
                }}
            />
        </div>
    )
}

export default withTranslation()(Filter)
