package com.viettel.gym_cms.service.impl;

import com.viettel.gym_cms.domain.Asset;
import com.viettel.gym_cms.domain.Trainer;
import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.repo.AssetRepo;
import com.viettel.gym_cms.repo.TrainerRepo;
import com.viettel.gym_cms.service.TrainerService;
import com.viettel.gym_cms.utils.Constant;
import com.viettel.gym_cms.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;

@RequiredArgsConstructor
@Slf4j
@Service
public class TrainerServiceImpl implements TrainerService {

    @Value("${app.files.path}")
    private String rootPath;

    @Value("${app.dns.path}")
    private String dnsUrl;

    private final TrainerRepo trainerRepo;
    private final AssetRepo assetRepo;


    @Override
    public ResponseDto getAll(Pageable pageable, Integer status, String name) {

        Page<Trainer> page;
        if(name == null){
            name = "";
        }
        if(status == null){
            page = trainerRepo.findByNameEnContainingIgnoreCase(pageable, name);
        }else{
            page = trainerRepo.findByNameEnContainingIgnoreCaseAndStatus(pageable, name, status);
        }

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("items", page.getContent())
                .onPushData("totalElements", page.getTotalElements())
                .onPushData("totalPages", page.getTotalPages())
                .build();
    }

    @Override
    public ResponseDto update(Trainer trainer) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        Trainer oldTrainer = trainerRepo.findById(trainer.getId()).orElse(null);

        if(oldTrainer != null){
            if(!ObjectUtils.isEmpty(trainer.getNameEn()) && !trainer.getNameEn().equals(oldTrainer.getNameEn())){
                oldTrainer.setNameEn(trainer.getNameEn());
            }

            if(!ObjectUtils.isEmpty(trainer.getNameLc()) && !trainer.getNameLc().equals(oldTrainer.getNameLc())){
                oldTrainer.setNameLc(trainer.getNameLc());
            }

            if(!ObjectUtils.isEmpty(trainer.getDescEn()) && !trainer.getDescEn().equals(oldTrainer.getDescEn())){
                oldTrainer.setDescEn(trainer.getDescEn());
            }

            if(!ObjectUtils.isEmpty(trainer.getDescLc()) && !trainer.getDescLc().equals(oldTrainer.getDescLc())){
                oldTrainer.setDescLc(trainer.getDescLc());
            }

            if(!ObjectUtils.isEmpty(trainer.getStatus()) && !trainer.getStatus().equals(oldTrainer.getStatus())){
                oldTrainer.setStatus(trainer.getStatus());
            }

            if(!ObjectUtils.isEmpty(trainer.getImageUrl()) && !trainer.getImageUrl().equals(oldTrainer.getImageUrl())){
                String base64En = trainer.getImageUrl();
                String extEn = FileUtil.getExtFile(base64En);
                trainer.setImageUrl(base64En.substring(base64En.indexOf(",")+1));
                String imageEn = FileUtil.generateFileName(oldTrainer.getId(), Constant.LANGUAGE_EN, extEn);

                try {
                    FileUtil.base64ToFile(trainer.getImageUrl(), rootPath + imageEn);
                    Asset assetImageLc = Asset.builder()
                            .id(assetRepo.getID())
                            .createdAt(assetRepo.currentDate())
                            .name(imageEn)
                            .link("/" + imageEn)
                            .status(Constant.STATUS_ACTIVE)
                            .type(Constant.FILE_TYPE)
                            .build();

                    assetRepo.save(assetImageLc);
                    oldTrainer.setImageUrl(dnsUrl + "/" + imageEn);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            oldTrainer.setUpdatedAt(trainerRepo.currentDate());
            oldTrainer.setUpdatedBy(userActive.getUserName());

            trainerRepo.save(oldTrainer);
        } else{
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_CANNOT_UPDATE)
                    .build();
        }
        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }

    @Override
    public ResponseDto create(Trainer trainer) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userActive = (User) authentication.getPrincipal();

        trainer.setId(trainerRepo.getID());
        trainer.setCreatedAt(trainerRepo.currentDate());
        trainer.setStatus(Constant.STATUS_ACTIVE);
        trainer.setCreatedBy(userActive.getUserName());

        String base64 = trainer.getImageUrl();
        String ext = FileUtil.getExtFile(base64);
        trainer.setImageUrl(base64.substring(base64.indexOf(",")+1));
        String image = FileUtil.generateFileName(trainer.getId(), Constant.LANGUAGE_EN, ext);

        try {
            FileUtil.base64ToFile(trainer.getImageUrl(), rootPath + image);

            Asset assetImage = Asset.builder()
                    .id(assetRepo.getID())
                    .createdAt(assetRepo.currentDate())
                    .name(image)
                    .link("/" + image)
                    .status(Constant.STATUS_ACTIVE)
                    .type(Constant.FILE_TYPE)
                    .build();

            assetRepo.save(assetImage);
            trainer.setImageUrl(dnsUrl + "/" + image);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        trainerRepo.save(trainer);

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .build();
    }
}
