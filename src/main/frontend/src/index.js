import React from 'react';
import ReactDOM from 'react-dom';
import './utils/env'
import reportWebVitals from './reportWebVitals'

import {applyMiddleware, createStore} from 'redux'

import {Provider} from 'react-redux'
import {Redirect, Route, Router, Switch} from "react-router-dom"
import createSagaMiddleware from 'redux-saga'

import rootReducer from './redux/reducers'

import "tailwindcss/tailwind.css"
import './assets/boxicons-2.0.7/css/boxicons.min.css'
import './assets/css/grid.css'
import './assets/css/theme.css'
import 'antd/dist/antd.css'
import './assets/css/index.scss'


import Routes from './router'
import rootSaga from "./redux/saga"

import './locales/i18n';
import {createBrowserHistory} from "history";

const sagaMiddleware = createSagaMiddleware()

export const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware),
)

sagaMiddleware.run(rootSaga)

document.title = 'Gym Admin'

const hist = createBrowserHistory();

ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            <Router history={hist}>
                <Switch>
                    <Route path="/GYM_CMS/" component={Routes}/>
                    <Redirect from="/" to="/GYM_CMS/"/>
                </Switch>
            </Router>
        </React.StrictMode>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
