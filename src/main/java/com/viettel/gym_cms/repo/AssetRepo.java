package com.viettel.gym_cms.repo;

import com.viettel.gym_cms.domain.Asset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssetRepo extends JpaRepository<Asset, String>, CommonRepo {
    
    Page<Asset> findByParentIdAndNameContainingIgnoreCaseOrderByCreatedAtDesc(Pageable pageable, String parentId, String name);

    Page<Asset> findByParentIdAndNameContainingIgnoreCaseAndStatusOrderByCreatedAtDesc(Pageable pageable, String parentId, String name, Integer status);

    List<Asset> findByParentIdAndTypeAndStatusOrderByCreatedAtDesc(String parentId, Integer type, Integer status);
    List<Asset> findByParentIdIsNullAndTypeAndStatusOrderByCreatedAtDesc(Integer type, Integer status);
    List<Asset> findByParentIdAndTypeOrderByCreatedAtDesc(String parentId, Integer type);
    List<Asset> findByParentIdIsNullAndTypeOrderByCreatedAtDesc(Integer type);
    List<Asset> findByNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(String name, Integer status, Integer type);

    Page<Asset> findByNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(Pageable pageable, String name, Integer status, Integer type);

    Page<Asset> findByParentIdAndNameContainingIgnoreCaseAndTypeOrderByCreatedAtDesc(Pageable pageable, String parentId, String name, Integer type);
    Page<Asset> findByParentIdIsNullAndNameContainingIgnoreCaseAndTypeOrderByCreatedAtDesc(Pageable pageable, String name, Integer type);

    Page<Asset> findByParentIdAndNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(Pageable pageable, String parentId, String name, Integer status, Integer type);
    Page<Asset> findByParentIdIsNullAndNameContainingIgnoreCaseAndStatusAndTypeOrderByCreatedAtDesc(Pageable pageable, String name, Integer status, Integer type);


}
