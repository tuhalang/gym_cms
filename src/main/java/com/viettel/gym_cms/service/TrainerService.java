package com.viettel.gym_cms.service;

import com.viettel.gym_cms.domain.Trainer;
import com.viettel.gym_cms.dto.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface TrainerService {

    ResponseDto getAll(Pageable pageable, Integer status, String name);

    ResponseDto update(Trainer trainer);

    ResponseDto create(Trainer trainer);
}
