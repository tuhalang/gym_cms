package com.viettel.gym_cms.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "lesson")
public class Lesson {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "category_id")
    private String categoryId;
    @Column(name = "trainer_id")
    private String trainerId;
    @Column(name = "name_en")
    private String nameEn;
    @Column(name = "name_lc")
    private String nameLc;
    @Column(name = "image_url_en", length = 1000)
    private String imageUrlEn;
    @Column(name = "image_url_lc", length = 1000)
    private String imageUrlLc;
    @Column(name = "desc_en", length = 4000)
    private String descEn;
    @Column(name = "desc_lc", length = 4000)
    private String descLc;
    @Column(name = "content_en", length = 4000)
    private String contentEn;
    @Column(name = "content_lc", length = 4000)
    private String contentLc;
    @Column(name = "lesson_level")
    private String level;
    @Column(name = "intensity")
    private String intensity;
    @Column(name = "order_number")
    private Integer orderNumber;
    @Column(name = "status")
    private Integer status;
    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_at")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "updated_by")
    private String updatedBy;

    @Transient
    private String categoryName;
    @Transient
    private String trainerName;


}
