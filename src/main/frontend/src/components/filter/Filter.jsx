import React from "react";

import './filter.scss';
import {Col, Input, Row, DatePicker} from "antd";
import {SearchOutlined} from "@ant-design/icons"
import {useDispatch} from "react-redux";
import {paramsUrl, selectedDate} from "../function";
import moment from "moment";

function Filter({onFilter, filter}) {

    const dispatch = useDispatch()
    const [loadingLoad, setLoadingLoad] = React.useState(false)
    const __filter = React.useRef({...paramsUrl.get()})
    const __timeOut = React.useRef()

    function onChangeFilter(key, value) {
        if (key === "date") {
            __filter.current.from = value[0] || undefined
            __filter.current.to = value[1] || undefined
        } else {
            __filter.current[key] = value
        }
        if (__timeOut.current) {
            clearTimeout(__timeOut.current)
        }
        __timeOut.current = setTimeout(() => {
            onFilter(__filter.current)
        }, 100)
    }

    return (
        <div className="flex justify-end filter">
            <Row gutter={12}>
                <Col className="mb-4">
                    <Input
                        prefix={<SearchOutlined/>}
                        placeholder="Order ID/ Tracking Number"
                        // defaultValue={search}
                        // onChange={({ target: { value } }) =>
                        //     onChangeFilter("search", value ? value : undefined)
                        //}
                    />
                </Col>

                <Col className="mb-4">
                    <DatePicker.RangePicker
                        ranges={selectedDate()}
                        defaultValue={
                            filter.from ? [moment(filter.from), moment(filter.to)] : undefined
                        }
                        className="w-full"
                        onChange={(value, dateString) =>
                            onChangeFilter("date", dateString)
                        }
                    />
                </Col>
            </Row>
        </div>
    )
}

export default Filter;
