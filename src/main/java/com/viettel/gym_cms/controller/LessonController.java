package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.domain.Lesson;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.LessonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class LessonController {

    private final LessonService lessonService;

    @RequestMapping(value = "/lesson", method = RequestMethod.POST)
    ResponseDto create(@RequestBody Lesson lesson){
        return lessonService.create(lesson);
    }

    @RequestMapping(value = "/lesson", method = RequestMethod.PUT)
    ResponseDto update(@RequestBody Lesson lesson){
        return lessonService.update(lesson);
    }

    @RequestMapping(value = "/lesson", method = RequestMethod.GET)
    ResponseDto getAll(@RequestParam Integer page,
                       @RequestParam Integer size,
                       @RequestParam(required = false) String name,
                       @RequestParam(required = false) String categoryId,
                       @RequestParam(required = false) String trainerId,
                       @RequestParam(required = false) Integer status){
        Pageable pageable = PageRequest.of(page-1, size, Sort.by("orderNumber").ascending());
        return lessonService.getAll(pageable, status, name, categoryId, trainerId);
    }
}
