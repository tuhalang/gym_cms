import React, {useEffect, useRef} from 'react'
import './index.scss'
import {useSelector} from "react-redux";
import {Button, Table, Space} from "antd";
import {paramsUrl} from "../../components/function";
import {useDispatch} from "react-redux";
import {listUsers, updateUser} from "../../redux/actions";
import _ from "lodash";
import ResetPassword from "../../components/modal/ResetPassword";
import Filter from "./Filter";

const Users = () => {

    const dispatch = useDispatch()
    const state = useSelector(state => state.common)
    const themeReducer = useSelector(state => state.ThemeReducer)
    const [resetting, setResetting] = React.useState(null)

    const __pagination = useRef({
        pageNumber: 1,
        pageSize: 10,
        total: state.totalElements,
        role: "",
        ...paramsUrl.get(),
    })

    function fetch({}) {
        dispatch(listUsers({
            page: __pagination.current.pageNumber,
            size: __pagination.current.pageSize,
            role: __pagination.current.role && __pagination.current.role !== "" ? __pagination.current.role : null,
            key: ""
        }))
    }

    const columns = [
        {
            title: 'No',
            width: 7,
            dataIndex: 'no',
            key: 'no',
            fixed: 'left',
        },
        {
            title: 'Username',
            width: 20,
            dataIndex: 'userName',
            key: 'userName',
            fixed: 'left',
        },
        {
            title: 'Role',
            dataIndex: 'role',
            key: 'role',
            width: 10,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            width: 10,
            render: (record) => record === 1 ? "Active" : "Inactive"
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: 'createdAt',
            width: 15,
        },
        {
            title: 'Created By',
            dataIndex: 'createdBy',
            key: 'createdBy',
            width: 20,
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 30,
            render: (_, {id, status, userName}) => <Space size="middle" className="action-user">
                <Button danger={status === 0 ? false : true}
                        size={'small'} onClick={() => {
                    dispatch(updateUser({
                        user: {
                            id,
                            userName,
                            status: status ? 0 : 1
                        },
                        callback: () => {
                            fetch({})
                        }
                    }))
                }}>{status === 1 ? "Disable" : "Enable"}</Button>
                <Button type="primary" size={'small'} onClick={() => setResetting({id, userName})}>Reset Pass</Button>
            </Space>,
        },
    ];

    function onFilter(params) {
        let newParams = {
            ...params,
        }
        __pagination.current = {
            ...__pagination.current,
            pageNumber: 1,
            ...newParams,
        }
        fetch({})
    }

    async function changePage(pageNumber, pageSize) {
        __pagination.current.pageNumber = pageNumber
        __pagination.current.pageSize = pageSize
        fetch({})
    }

    useEffect(() => {
        fetch({})
    }, [])

    useEffect(() => {
        __pagination.current.total = state.totalElements
    }, [state.totalElements])

    return (
        <div className="users">
            <h2 className={`page-header ${themeReducer.word}`}>
                users
            </h2>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <Filter
                            isAll={true}
                            onFilter={onFilter}
                            role={__pagination.role}
                            handleOk={() => {
                                fetch({})
                            }}
                            onFetch={() => fetch({})}
                            filter={__pagination}
                        />
                        <div className="card__body">
                            <Table
                                columns={columns}
                                dataSource={_.map(state.items, (e, idx) => _.assign(e, {no: idx + 1})) || []}
                                scroll={{x: 300}}
                                pagination={{...__pagination.current, onChange: changePage}}
                                bordered
                            />
                            {/*<Pagination pagination={__pagination.current} onChange={changePage}/>*/}
                        </div>
                    </div>
                </div>
            </div>
            <ResetPassword
                visible={resetting}
                id={resetting ? resetting.id : ''}
                userName={resetting ? resetting.userName : ''}
                onClose={() => setResetting('')}
                onSuccess={() => {
                    setResetting('')
                }}
            />
        </div>
    )
}

export default Users
