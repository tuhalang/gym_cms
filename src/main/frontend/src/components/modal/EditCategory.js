import React, {useEffect} from "react";
import {AutoComplete, Button, Form, Input, Modal} from "antd";
import '../../assets/css/modal_repair.css'
import {useDispatch, useSelector} from "react-redux";
import {createCategory, listCategories, trainer, updateCategory, updateCategoryLocal} from "../../redux/actions";
import UploadImage from "../upload/UploadImage";

function EditCategory({visible, onClose, category, reLoad, create}) {
    const dispatch = useDispatch()
    const [form] = Form.useForm()
    const [loading, setLoading] = React.useState(false)
    const [uploading, setUploading] = React.useState('')
    const [imageUrlEn, setImageUrlEn] = React.useState('')
    const [imageUrlLc, setImageUrlLc] = React.useState('')
    const trainerState = useSelector(state => state.trainer)
    const [trainerInput, setTrainerInput] = React.useState({})


    const handleCreate = (values) => {
        if (create) {
            dispatch(createCategory({
                category: {
                    ...values,
                    parentId: category.id,
                    imageUrlEn,
                    imageUrlLc,
                    trainerId: trainerInput.id,
                },
                callback: () => {
                    setLoading(false)
                    reLoad()
                },
                callbackSuccess: () => {
                    onClose()
                    dispatch(listCategories({
                        name: "",
                        page: 1,
                        size: 10,
                        status: 1,
                        parentId: category.id,
                    }))
                }
            }))
        } else {
            dispatch(updateCategory({
                category: {
                    ...values,
                    id: category.id,
                    imageUrlEn,
                    imageUrlLc,
                    trainerId: trainerInput.id,
                },
                callback: () => {
                    setLoading(false)
                    reLoad()
                },
                callbackSuccess: () => {
                    onClose()
                }
            }))
        }
    }

    const onSelectTrainer = (_, {id, value}) => {
        console.log(id, value, "trainer")
        setTrainerInput({id, value})
    }

    const onFetchTrainer = (data) => {
        dispatch(trainer({
            name: data,
            page: 1,
            size: 10,
            status: 1,
        }))
    }

    useEffect(() => {
        if (!create && category && form) {
            form.setFieldsValue({...category})
            setImageUrlEn(category.imageUrlEn)
            setImageUrlLc(category.imageUrlLc)
        }
        onFetchTrainer("")
    }, [category])

    return (
        <Modal
            title={create ? "Create a new category" : "Update category"}
            visible={visible}
            // key={"edit-category"}
            onCancel={onClose}
            maskClosable={false}
            destroyOnClose={true}
            name="upload_image"
            centered={true}
            width={650}
            footer={[
                <Button type={'dashed'} onClick={() => {
                    form.resetFields()
                    setImageUrlLc('')
                    setImageUrlEn('')
                    onClose()
                }}>Cancel</Button>,
                <Button type={'primary'} htmlType={'submit'} loading={loading}
                        form={"create-category"}>{create ? 'Create' : 'Update'}</Button>
            ]}
            className="create-user is-floating-label"
        >
            <Form
                layout="vertical"
                name="basic"
                form={form}
                id={"create-category"}
                initialValues={{remember: true}}
                onFinish={handleCreate}
            >
                <Form.Item
                    label="Category (En)"
                    name="nameEn"
                    rules={[{required: true, message: "Please input category name !"}]}
                >
                    <Input placeholder={'Category En'}/>
                </Form.Item>
                <Form.Item
                    label="Category (Lc)"
                    name="nameLc"
                    rules={[{required: true, message: "Please input category name !"}]}
                >
                    <Input placeholder={'Category Lc'}/>
                </Form.Item>
                <Form.Item
                    label="Description (En)"
                    name="descEn"
                    rules={[{required: true, message: "Please input description!"}]}
                >
                    <Input.TextArea placeholder={'Description En'}/>
                </Form.Item>
                <Form.Item
                    label="Description (Lc)"
                    name="descLc"
                    rules={[{required: true, message: "Please input description!"}]}
                >
                    <Input.TextArea placeholder={'Description Lc'}/>
                </Form.Item>
                <div className="flex">
                    <Form.Item
                        label="Image (En)"
                        name="imageUrlEn"
                        className="flex-grow"
                        rules={[{required: true, message: "Please input image url!"}]}
                    >
                        <Input placeholder={'Image En'} disabled/>
                    </Form.Item>
                    <Button className="m-3" onClick={() => setUploading("imageUrlEn")}><i
                        className='bx bxs-cloud-upload'></i></Button>
                </div>
                <div className="flex justify-between">
                    <Form.Item
                        label="Image (Lc)"
                        name="imageUrlLc"
                        className="flex-grow"
                        rules={[{required: true, message: "Please input image url!"}]}
                    >
                        <Input placeholder={'Image Lc'} disabled/>
                    </Form.Item>
                    <Button className="m-3" onClick={() => setUploading("imageUrlLc")}><i
                        className='bx bxs-cloud-upload'></i></Button>
                </div>

                <Form.Item
                    label="Trainer"
                    name="trainerId"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    <AutoComplete
                        className="w-full"
                        options={trainerState.items.map(item => ({
                            label: item.nameEn,
                            value: item.nameEn,
                            id: item.id,
                        }))}
                        allowClear={true}
                        onSelect={onSelectTrainer}
                        onSearch={onFetchTrainer}
                        placeholder={'--Trainer--'}
                    />
                </Form.Item>

                <Form.Item
                    label="Order number"
                    name="orderNumber"
                    rules={[{required: true, message: "Please input order number!"}]}
                >
                    <Input placeholder={'Order number'}/>
                </Form.Item>
            </Form>
            <Modal
                title={"Upload image"}
                destroyOnClose={true}
                visible={uploading && uploading != ""}
                onCancel={() => setUploading('')}
                footer={[]}
            >
                <UploadImage
                    callback={async ({files}) => {
                        // handleUpload(files[0])
                        let name = files[0].name
                        let file = await toBase64(files[0])
                        if (uploading === "imageUrlEn") {
                            setImageUrlEn(file)
                        } else setImageUrlLc(file)
                        form.setFieldsValue({
                            [uploading]: name
                        })
                        setUploading('')
                    }}
                />
            </Modal>
        </Modal>
    )
}

export default EditCategory

const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    }
);
