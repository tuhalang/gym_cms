import Client from "../client/Client"

const resource = "/secure"

const list = (params) => {
    return Client.get(`${resource}/asset`, {params})
}

const deleteAsset = (params) => {
    return Client.delete(`${resource}/asset`, {params})
}

const create = (data) => {
    const form = new FormData();
    for (const [key, value] of Object.entries(data)) {
        form.set(key, value)
    }
    return Client.post(`${resource}/asset`, form, {headers: {"Content-Type": "multipart/form-data"}})
}

const assetsRepository = {
    list,
    create,
    deleteAsset,
}
export default assetsRepository
