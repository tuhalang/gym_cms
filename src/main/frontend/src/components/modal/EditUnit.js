import React from "react";
import {AutoComplete, Button, Form, Input, Modal} from "antd";
import "./index.scss"
import {
    createUnit,
    lesson,
    updateUnit
} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";

function EditUnit({visible, onClose, defaultValue}) {
    const ref = React.useRef()
    const dispatch = useDispatch()
    const lessonState = useSelector(state => state.lesson)
    const [loading, setLoading] = React.useState(false)
    const [lessonInput, setLessonInput] = React.useState({})

    const onFinish = (values) => {
        setLoading(true)
        if (defaultValue && defaultValue.id !== "") {
            dispatch(updateUnit({
                ...values,
                id: defaultValue.id,
                lessonId: lessonInput.id,
                callback: () => {
                    setLoading(false)
                    onClose()
                }
            }))
        } else {
            dispatch(createUnit({
                ...values,
                lessonId: lessonInput.id,
                callback: () => {
                    setLoading(false)
                    onClose()
                }
            }))
        }
    }

    const onSelectLesson = (_, {id, value}) => {
        setLessonInput({id, value})
    };

    const onFetchLesson = (data) => {
        dispatch(lesson({
            name: data,
            page: 1,
            size: 10,
            status: null,
        }))
    };

    React.useEffect(() => {
        if (defaultValue && defaultValue.id !== "") {
            console.log(defaultValue, "defaultValue")
            if (ref.current)
                ref.current.setFieldsValue({
                    ...defaultValue,
                    lessonId: defaultValue.lesson ? defaultValue.lesson.nameEn : "",
                })
            else setTimeout(() => ref.current.setFieldsValue({
                    ...defaultValue,
                    lessonId: defaultValue.lesson ? defaultValue.lesson.nameEn : "",
                }),
                50
            )
        }

        onFetchLesson("")
    }, [defaultValue])

    return (
        <Modal
            visible={visible}
            width={900}
            onCancel={onClose}
            centered
            maskClosable={false}
            destroyOnClose={true}
            footer={[<Button type={'primary'} htmlType={'submit'} loading={loading}
                             form={"create-user"}>Submit</Button>]}
            className="create-user is-floating-label w-full flex flex-column"
        >
            <span className="title-create mb-5">{!(defaultValue && defaultValue.id !== "") ? "Create a new unit  " : "Update unit"}</span>
            <br/>
            <br/>
            <Form
                layout="vertical"
                name="basic"
                ref={ref}
                id={"create-user"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Name"
                    name="nameEn"
                    rules={[{required: true, message: "Please input your name!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Name(Lc)"
                    name="nameLc"
                    rules={[{required: true, message: "Please input your name!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Content"
                    name="contentEn"
                    rules={[
                        {required: true, message: "Please input your content!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Content(Lc)"
                    name="contentLc"
                    rules={[
                        {required: true, message: "Please input your content!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Video url"
                    name="videoUrlEn"
                    rules={[
                        {
                            required: true,
                            message: "Please input your desc!",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Video url(Lc)"
                    name="videoUrlLc"
                    className="flex-grow"
                    rules={[{required: false, message: "Please input your desc!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Estimate time"
                    name="estimateTime"
                    rules={[
                        {
                            required: true,
                            message: "Please input your estimate time!",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Order Number"
                    name="orderNumber"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Lesson"
                    name="lessonId"
                    rules={[
                        {
                            required: false,
                        },
                    ]}
                >
                    <AutoComplete
                        className="w-full"
                        options={lessonState.items.map(item => ({
                            label: item.nameEn,
                            value: item.nameEn,
                            id: item.id,
                        }))}
                        onSelect={onSelectLesson}
                        onSearch={onFetchLesson}
                        placeholder={'--Lesson--'}
                        value={lessonInput.value || ""}
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default EditUnit

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});
