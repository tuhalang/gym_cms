import React, {useRef} from "react"
import {
    Row,
    Col,
    Input,
    Tabs,
    Space,
    Button,
} from "antd"
import {SearchOutlined} from "@ant-design/icons"
import {withTranslation} from "react-i18next"
import {paramsUrl} from "../../components/function";
import {COMMON_STATUS} from "../../utils/constant";
import EditTrainer from "../../components/modal/EditTrainer";

const {TabPane} = Tabs
const dfColFilter = {sm: 24, md: 24, lg: 18, xl: 18}

const Filter = ({onFilter, filter, onFetch}) => {
    const [editing, setEditing] = React.useState(false)
    const __filter = useRef({
        ...paramsUrl.get()
    })

    function onChangeFilter(key, value) {
        __filter.current[key] = value
        onFilter(__filter.current)
    }

    const {name} = filter

    return (
        <div className="items-center m-4 card-filter">
            <Row>
                <Tabs
                    className="w-full"
                    defaultActiveKey={filter ? filter.status : "1"}
                    onChange={(value) =>
                        onChangeFilter("status", value)
                    }
                >
                    {COMMON_STATUS.length > 0 &&
                    COMMON_STATUS.map(({id, code}) => {
                        return (
                            <TabPane
                                tab={<span className="capitalize">{code}</span>}
                                key={id.toString()}
                            />
                        )
                    })}
                </Tabs>
            </Row>
            <div className="flex justify-between">
                <Row gutter={12}>
                    <Col {...dfColFilter} className="mb-4">
                        <Input
                            prefix={<SearchOutlined/>}
                            placeholder="Name..."
                            defaultValue={name}
                            onChange={({target: {value}}) =>
                                onChangeFilter("name", value ? value : "")
                            }
                        />
                    </Col>
                </Row>
                <Space className="items-start">
                    <Button type={"primary"} onClick={() => setEditing(true)}>Create</Button>
                </Space>
            </div>
            <EditTrainer
                visible={editing}
                onClose={() => {
                    setEditing(false)
                }}
                defaultValue={undefined}
            />
        </div>
    )
}

export default withTranslation()(Filter)
