import React from 'react'

import './statuscard.css'
import {useSelector} from "react-redux";

const StatusCard = props => {
    const themeReducer = useSelector(state => state.ThemeReducer)

    return (
        <div className={`status-card ${themeReducer.word}`}>
            <div className="status-card__icon">
                <i className={props.icon}></i>
            </div>
            <div className="status-card__info">
                <h4 className={themeReducer.word}>{props.count}</h4>
                <span>{props.title}</span>
            </div>
        </div>
    )
}

export default StatusCard
