package com.viettel.gym_cms.service;

import com.viettel.gym_cms.dto.ResponseDto;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface AssetService {

    ResponseDto upload(String parentId, Integer type, String name, MultipartFile file);

    ResponseDto retrieve(Pageable pageable, String parentId, String name, Integer status);

    ResponseDto delete(String id);
}
