import React, {useState} from "react"
import {Form, Input, Button, Row, Col, message, notification, Select} from "antd"
import {Link} from "react-router-dom"
import getFactory from "../../api"
import wordwideBg from "../../assets/images/wordwide.svg"
import "./index.scss"
import {ROLES} from "../../utils/constant";
import {useDispatch} from "react-redux";
import {register} from "../../redux/actions";

const apiUser = getFactory("user")
const SignUp = () => {
    const ref = React.createRef()
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false)

    async function handleSubmit(values) {
        dispatch(register({...values}))
    }

    function onFinishFailed(params) {
    }

    React.useEffect(() => {
        ref.current.setFieldsValue({
            role: ROLES[0]
        })
    }, [])

    return (
        <Row className="page-container vcs-page">
            <Col xs={24} sm={12} md={12} lg={10}>
                <div className="logo flex items-end">
                    <div className="logo-vcs"/>
                    <div className="logo-text ml-2"> SPORT NEWS</div>
                </div>
                <div className="page_login">
                    <div className="login_form is-floating-label">
                        <div className="text4xl f-500 mb-4">Register</div>
                        <Form
                            layout="vertical"
                            name="basic"
                            ref={ref}
                            initialValues={{remember: true}}
                            onFinish={handleSubmit}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Username"
                                name="username"
                                rules={[
                                    {required: true, message: "Please input your username!"},
                                ]}
                            >
                                <Input placeholder="Username..."/>
                            </Form.Item>
                            <Form.Item
                                label="Role"
                                name="role"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your role!",
                                    },
                                ]}
                            >
                                <Select>
                                    {ROLES.map(item => <Select.Option value={item}>{item}</Select.Option>)}
                                </Select>
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[
                                    {required: true, message: "Please input your password!"},
                                ]}
                            >
                                <Input.Password/>
                            </Form.Item>
                            <Form.Item
                                label="Confirm password"
                                name="confirm_password"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your confirm password!",
                                    },
                                ]}
                            >
                                <Input.Password/>
                            </Form.Item>

                            <Form.Item>
                                <Button
                                    className="w-right vcs-button w-200"
                                    type="primary"
                                    htmlType="submit"
                                    loading={loading}
                                >
                                    Register
                                </Button>
                                <Link to="/login" className="ml-4">
                                    Login
                                </Link>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </Col>
            <Col xs={0} sm={12} md={12} lg={14}>
                <div className="page_info">
                    <img className="page_img" alt="" src={wordwideBg}/>
                    <div className="container">
                        <h1>Vận chuyển hàng e-commerce toàn cầu</h1>
                        <p>
                            VCS cung cấp dịch vụ vận chuyển e-commerce từ Việt Nam, Trung Quốc
                            tới tất cả các nước trên thế giới dù chỉ là một đơn hàng. Hàng hoá
                            có thể đi thẳng từ nhà sản xuất tới tận tay người tiêu dùng.
                        </p>
                    </div>
                </div>
            </Col>
        </Row>
    )
}

export default SignUp
