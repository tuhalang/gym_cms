import React from "react";
import {AutoComplete, Button, Form, Input, Modal} from "antd";
import "./index.scss"
import {
    createTrainer,
    createUnit,
    lesson, updateTrainer,
    updateUnit
} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import UploadImage from "../upload/UploadImage";

function EditTrainer({visible, onClose, defaultValue}) {
    const ref = React.useRef()
    const dispatch = useDispatch()
    const [loading, setLoading] = React.useState(false)
    const [uploading, setUploading] = React.useState(false)
    const [imageUrl, setImageUrl] = React.useState("")

    const onFinish = (values) => {
        setLoading(true)
        if (defaultValue && defaultValue.id !== "") {
            dispatch(updateTrainer({
                ...values,
                id: defaultValue.id,
                imageUrl,
                callback: () => {
                    setLoading(false)
                    onClose()
                }
            }))
        } else {
            dispatch(createTrainer({
                ...values,
                imageUrl,
                callback: () => {
                    setLoading(false)
                    onClose()
                }
            }))
        }
    }

    React.useEffect(() => {
        if (defaultValue && defaultValue.id !== "") {
            console.log(defaultValue, "defaultValue")
            if (ref.current)
                ref.current.setFieldsValue({
                    ...defaultValue,
                })
            else setTimeout(() => ref.current.setFieldsValue({
                    ...defaultValue,
                }),
                50
            )
        }
    }, [defaultValue])

    return (
        <Modal
            visible={visible}
            width={900}
            onCancel={onClose}
            centered
            maskClosable={false}
            destroyOnClose={true}
            footer={[<Button type={'primary'} htmlType={'submit'} loading={loading}
                             form={"create-user"}>Submit</Button>]}
            className="create-user is-floating-label w-full flex flex-column"
        >
            <span className="title-create mb-5">{!(defaultValue && defaultValue.id !== "") ? "Create a new trainer" : "Update trainer"}</span>
            <br/>
            <br/>
            <Form
                layout="vertical"
                name="basic"
                ref={ref}
                id={"create-user"}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Name"
                    name="nameEn"
                    rules={[{required: true, message: "Please input your name!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Name(Lc)"
                    name="nameLc"
                    rules={[{required: true, message: "Please input your name!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Description"
                    name="descEn"
                    rules={[
                        {required: true, message: "Please input your content!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <Form.Item
                    label="Description(Lc)"
                    name="descLc"
                    rules={[
                        {required: true, message: "Please input your content!"},
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
                <div className="flex">
                    <Form.Item
                        label="Image"
                        name="imageUrl"
                        className="flex-grow"
                        rules={[{required: false, message: "Please input your image!"}]}
                    >
                        <Input disabled/>
                    </Form.Item>
                    <Button className="m-3" onClick={() => setUploading(true)}><i
                        className='bx bxs-cloud-upload'></i></Button>
                </div>
            </Form>
            <Modal
                title={"Upload image"}
                visible={uploading}
                destroyOnClose={true}
                onCancel={() => setUploading(null)}
                footer={[]}
            >
                <UploadImage
                    callback={async ({files}) => {
                        let name = files[0].name
                        let file = await toBase64(files[0])
                        setImageUrl(file)
                        ref.current.setFieldsValue({
                            imageUrl: name
                        })
                        setUploading(null)
                    }}
                />
            </Modal>
        </Modal>
    )
}

export default EditTrainer

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});
