import {useEffect, useReducer} from "react"
import {useDispatch, useSelector} from "react-redux";
import {listAsset} from "../../redux/actions";
import {SELECT_FOLDER, UPDATE_FOLDER} from "../../redux/actions/action_types";

export const ROOT_FOLDER = {name: "My folder", id: null, path: []}

export default function useFolder({}) {

    const dispatch = useDispatch()

    const state = useSelector(state => state.common)
    // useEffect(() => {
    //     dispatch({type: SELECT_FOLDER, payload: {folder, folderId}})
    // }, [folderId, folder])

    useEffect(() => {
        if (state.folderId == null) {
            return dispatch({
                type: UPDATE_FOLDER,
                payload: {folder: ROOT_FOLDER}
            })
        }

    }, [state.folderId])

    useEffect(() => {
        dispatch(listAsset({
            parentId: state.folderId,
            name: '',
            page: 1,
            size: 10,
            status: 1,
        }))
    }, [state.folderId])

    return state
}
