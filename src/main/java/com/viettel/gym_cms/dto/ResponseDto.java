package com.viettel.gym_cms.dto;

import com.viettel.gym_cms.utils.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto {

    private String errorCode = Constant.ERROR_CODE_NOK;
    private String message;
    private LinkedHashMap<String, Object> data;

    public void pushData(String key, Object value){
        if(this.data == null){
            this.data = new LinkedHashMap<>();
        }
        this.data.put(key, value);
    }

    public static ResponseDtoBuilder builder(){
        return new ResponseDtoBuilder();
    }
}
