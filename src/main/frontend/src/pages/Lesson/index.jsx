import React from "react";
import {useDispatch, useSelector} from "react-redux";
import Filter from "./Filter";
import {paramsUrl} from "../../components/function";
import TableLesson from "./TableLesson";
import {withTranslation} from "react-i18next";
import {lesson, updateLesson} from "../../redux/actions";
import EditLesson from "../../components/modal/EditLesson";

function Index({t}) {
    const dispatch = useDispatch()
    const themeReducer = useSelector(state => state.ThemeReducer)
    const {items, totalPages, totalElements} = useSelector(state => state.lesson)
    const [loading, setLoading] = React.useState(false)
    const [valueEditing, setValueEditing] = React.useState(undefined)
    const [editing, setEditing] = React.useState(false)

    const [__pagination, __setPagination] = React.useState({
        pageNumber: 1,
        pageSize: 10,
        total: 0,
        status: "-1",
        name: "",
        type: "",
        ...paramsUrl.get(),
    })

    const fetch = ({}) => {
        setLoading(true)
        switch (__pagination.status) {
            case "-1": //all
                dispatch(lesson({
                    ...__pagination,
                    page: __pagination.pageNumber,
                    size: __pagination.pageSize,
                    status: null,
                    callback: () => {
                        setLoading(false)
                    }
                }))
                break;
            default:
                dispatch(lesson({
                    ...__pagination,
                    page: __pagination.pageNumber,
                    size: __pagination.pageSize,
                    callback: () => {
                        setLoading(false)
                    }
                }))
                break;
        }
    }

    function onFilter(params) {
        __setPagination({
            ...__pagination,
            ...params,
        })
        // fetch({})
    }

    async function changePage(pageNumber, pageSize) {
        __setPagination({...__pagination, pageNumber, pageSize})
        // fetch({})
    }

    React.useEffect(() => {
        fetch({})
    }, [__pagination])

    return (
        <div>
            <h2 className={`page-header ${themeReducer.word}`}>
                lesson
            </h2>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <Filter
                            isAll={true}
                            onFilter={onFilter}
                            status={__pagination.status}
                            handleOk={() => {
                                fetch({})
                            }}
                            onFetch={() => fetch({})}
                            filter={__pagination}
                        />
                        <TableLesson
                            dataSource={items}
                            loading={loading}
                            onAction={(record) => {
                                setValueEditing(record)
                                setTimeout(() =>
                                    setEditing(true), 10)
                            }}
                            onDelete={(record) => {
                                dispatch(updateLesson({
                                    id: record.id,
                                    status: 0,
                                    callback: () => {
                                        fetch({})
                                    }
                                }))
                            }}
                            onRestore={(record) => {
                                dispatch(updateLesson({
                                    id: record.id,
                                    status: 1,
                                    callback: () => {
                                        fetch({})
                                    }
                                }))
                            }}
                            pagination={{...__pagination, onChange: changePage, total: totalElements, type: 'large'}}
                            t={t}
                        />
                    </div>
                </div>
            </div>
            <EditLesson
                key={new Date().getTime()}
                visible={editing}
                onClose={() => {
                    setEditing(false)
                    fetch({})
                    setValueEditing({})
                }}
                defaultValue={valueEditing}
            />
        </div>
    )
}

export default withTranslation()(Index)
