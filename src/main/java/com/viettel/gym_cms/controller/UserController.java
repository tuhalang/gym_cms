package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.domain.User;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class UserController {

    private final UserService userService;

    @Secured("ADMIN")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseDto getAll(@RequestParam Integer page,
                              @RequestParam Integer size,
                              @RequestParam(required = false) String role,
                              @RequestParam(required = false) Integer status,
                              @RequestParam(required = false) String key){
        Pageable pageable = PageRequest.of(page-1, size);
        return userService.getAll(pageable, status, key, role);
    }

    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public ResponseDto update(@RequestBody User user){
        return userService.update(user);
    }
}
