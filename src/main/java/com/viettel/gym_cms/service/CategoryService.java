package com.viettel.gym_cms.service;

import com.viettel.gym_cms.domain.Category;
import com.viettel.gym_cms.dto.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface CategoryService {

    ResponseDto create(Category category);

    ResponseDto update(Category category);

    ResponseDto retrieve(Pageable pageable, String parentId, String name, Integer status);

    ResponseDto search(Pageable pageable, String name, Integer status);

    ResponseDto getTree(String parentId);
}
