import Client from "../client/Client"

const resource = "/auth"

const register = (data) => {
    return Client.post(`${resource}/signUp`, data)
}

const login = (data) => {
    return Client.post(`${resource}/signIn`, data)
}

const list = (params) => {
    return Client.get(`/secure/users`, {params})
}

const update = (data) => {
    return Client.put(`/secure/users`, data)
}

const userRepository = {
    login,
    register,
    list,
    update,
}
export default userRepository
