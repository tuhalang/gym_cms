import React from 'react';
import {useDropzone} from 'react-dropzone';
import {Button, notification} from "antd";

function UploadFile({ callback }) {
    const {acceptedFiles, getRootProps, getInputProps} = useDropzone({
        // accept: 'image/jpeg, image/png, image/jpg, audio/*, video/*',
        multiple: false
    });

    const uploadFile = async (files) => {
        if (files == null || files.length == 0) return;
        callback({files})
        acceptedFiles.length = 0
    }

    const files = acceptedFiles.map(file => {
        return (
            <li key={file.path}>
                {file.path} - {file.size} bytes
            </li>
        )
    });

    return (
        <section className="container" style={{display: 'flex', justifyContent: 'center', flexDirection: 'column'}}>
            <div {...getRootProps({className: 'dropzone'})}
                 style={{
                     borderStyle: 'dotted',
                     borderWidth: 2,
                     borderColor: 'rgba(0,0,0,0.2)',
                     width: '100%',
                     display: 'flex',
                     justifyContent: 'center',
                     alignItems: 'center',
                     marginTop: '10%',
                     paddingTop: 16,
                     paddingBottom: 16,
                     marginBottom: 8,
                     borderRadius: 10,
                 }}>
                <input {...getInputProps()} />
                <div>
                    <span style={{color: 'rgba(0,0,0,0.4)', width: '100%'}}>Drag and drop or click here to upload your files</span>
                </div>
            </div>
            <aside>
                <h4>Selected file:</h4>
                <ul>{files}</ul>
                <Button type="primary" onClick={() => uploadFile(acceptedFiles)}
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.07)',
                            borderRadius: 8,
                            // padding: 8,
                            textAlign: 'center',
                            color: 'black',
                            width: '100%'
                        }}>
                    Upload
                </Button>
            </aside>
        </section>
    );
}

export default UploadFile;
