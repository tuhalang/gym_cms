import UserRepository from "./repository/UserRepository"
import AssetRepository from "./repository/AssetRepository"
import NewsRepository from "./repository/NewsRepository";
import CategoryRepository from "./repository/CategoryRepository"
import TrainerRepository from "./repository/TrainerRepository"
import LessonRepository from "./repository/LessonRepository"
import UnitRepository from "./repository/UnitRepository"

const repositories = {
    user: UserRepository,
    asset: AssetRepository,
    news: NewsRepository,
    category: CategoryRepository,
    trainer: TrainerRepository,
    lesson: LessonRepository,
    unit: UnitRepository,
}

export default function getFactory(name) {
    return repositories[name]
}
