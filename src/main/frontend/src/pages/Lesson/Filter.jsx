import React, {useRef} from "react"
import {
    Row,
    Col,
    Input,
    Tabs,
    Space,
    Button,
    AutoComplete,
    Tooltip,
} from "antd"
import {ClearOutlined, SearchOutlined} from "@ant-design/icons"
import {withTranslation} from "react-i18next"
import {paramsUrl} from "../../components/function";
import {COMMON_STATUS} from "../../utils/constant";
import {useDispatch, useSelector} from "react-redux";
import {searchCategories, trainer} from "../../redux/actions";
import {useHistory} from "react-router-dom";
import EditLesson from "../../components/modal/EditLesson";

const {TabPane} = Tabs
const dfColFilter = {sm: 12, md: 12, lg: 8, xl: 8}

const Filter = ({onFilter, filter, isAll, selectedRowKeys, t, handleOk, status, onFetch}) => {
    const dispatch = useDispatch()
    const category = useSelector(state => state.category)
    const trainerState = useSelector(state => state.trainer)
    const history = useHistory()
    const [editing, setEditing] = React.useState(false)
    const __filter = useRef({
        ...paramsUrl.get()
    })

    function onChangeFilter(key, value) {
        __filter.current[key] = value
        onFilter(__filter.current)
    }

    function handlerSubmit() {
        onFetch()
    }

    const {name} = filter

    const onSelect = (data, {id}) => {
        onChangeFilter("categoryId", id)
    };
    const onSelectTrainer = (data, {id}) => {
        onChangeFilter("trainerId", id)
    };
    const onFetchCategories = (data) => {
        dispatch(searchCategories({
            name: data,
            page: 1,
            size: 10,
            status: 1,
        }))
    };
    const onFetchTrainer = (data) => {
        dispatch(trainer({
            name: data,
            page: 1,
            size: 10,
            status: 1,
        }))
    }

    React.useEffect(() => {
        onFetchCategories("")
        onFetchTrainer("")
    }, [])

    const {items, type} = category

    return (
        <div className="items-center m-4 card-filter">
            <Row>
                {isAll && (
                    <Tabs
                        className="w-full"
                        defaultActiveKey={filter ? filter.status : "1"}
                        onChange={(value) =>
                            onChangeFilter("status", value)
                        }
                    >
                        {COMMON_STATUS.length > 0 &&
                        COMMON_STATUS.map(({id, code}) => {
                            return (
                                <TabPane
                                    tab={<span className="capitalize">{code}</span>}
                                    key={id.toString()}
                                />
                            )
                        })}
                    </Tabs>
                )}
            </Row>
            <div className="flex justify-between">
                <Row gutter={12}>
                    <Col {...dfColFilter} className="mb-4">
                        <Input
                            prefix={<SearchOutlined/>}
                            placeholder="Name..."
                            defaultValue={name}
                            onChange={({target: {value}}) =>
                                onChangeFilter("name", value ? value : "")
                            }
                        />
                    </Col>
                    <Col {...dfColFilter} className="mb-4">
                        {items && <AutoComplete
                            className="w-full"
                            options={items.map(item => ({
                                label: item.nameEn,
                                value: item.nameEn,
                                id: item.id,
                            }))}
                            clearIcon={<ClearOutlined onClick={() => onChangeFilter("categoryId", null)}/>}
                            allowClear={true}
                            onSelect={onSelect}
                            onSearch={onFetchCategories}
                            placeholder={'--Category--'}
                            value={filter.categoryId && items.find(item => item.id === filter.categoryId) ? items.find(item => item.id === filter.categoryId).nameEn : ""}
                        />}
                    </Col>
                    <Col {...dfColFilter} className="mb-4">
                        <AutoComplete
                            className="w-full"
                            options={trainerState.items.map(item => ({
                                label: item.nameEn,
                                value: item.nameEn,
                                id: item.id,
                            }))}
                            clearIcon={<ClearOutlined onClick={() => onChangeFilter("trainerId", null)}/>}
                            allowClear={true}
                            onSelect={onSelectTrainer}
                            onSearch={onFetchTrainer}
                            placeholder={'--Trainer--'}
                            value={filter.trainerId && trainerState.items.find(item => item.id === filter.trainerId) ? trainerState.items.find(item => item.id === filter.trainerId).nameEn : ""}
                        />
                    </Col>
                </Row>
                <Space className="items-start">
                    <Tooltip title="search">
                        <Button type="primary" shape="circle" icon={<SearchOutlined/>} onClick={handlerSubmit}/>
                    </Tooltip>
                    <Button type={"primary"} onClick={() => setEditing(true)}>Create</Button>
                </Space>
            </div>
            <EditLesson
                key={new Date().getTime()}
                visible={editing}
                onClose={() => {
                    setEditing(false)
                    onFetch()
                    }}
                defaultValue={undefined}
            />
        </div>
    )
}

export default withTranslation()(Filter)
