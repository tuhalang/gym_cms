package com.viettel.gym_cms.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "category")
public class Category {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "parent_id")
    private String parentId;
    @Column(name = "trainer_id")
    private String trainerId;
    @Column(name = "name_en")
    private String nameEn;
    @Column(name = "name_lc")
    private String nameLc;
    @Column(name = "order_number")
    private Integer orderNumber;
    @Column(name = "desc_en", length = 4000)
    private String descEn;
    @Column(name = "desc_lc", length = 4000)
    private String descLc;
    @Column(name = "image_url_en", length = 1000)
    private String imageUrlEn;
    @Column(name = "image_url_lc", length = 1000)
    private String imageUrlLc;
    @Column(name = "status")
    private Integer status;
    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_at")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "updated_by")
    private String updatedBy;

    @Transient
    private List<Category> children;
    @Transient
    private String key;
    @Transient
    private String title;
    @Transient
    private String value;
}
