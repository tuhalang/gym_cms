package com.viettel.gym_cms.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "unit")
public class Unit {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "lesson_id")
    private String lessonId;
    @Column(name = "name_en")
    private String nameEn;
    @Column(name = "name_lc")
    private String nameLc;
    @Column(name = "content_en")
    private String contentEn;
    @Column(name = "content_lc")
    private String contentLc;
    @Column(name = "video_url_en")
    private String videoUrlEn;
    @Column(name = "video_url_lc")
    private String videoUrlLc;
    @Column(name = "order_number")
    private Integer orderNumber;
    @Column(name = "estimate_time")
    private Integer estimateTime;
    @Column(name = "status")
    private Integer status;
    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_at")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "updated_by")
    private String updatedBy;
}
