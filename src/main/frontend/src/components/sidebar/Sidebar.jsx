import React from 'react'

import {Link, useHistory} from 'react-router-dom'

import './sidebar.css'

import logo from '../../assets/images/logo.png'

import sidebar_items from '../../assets/JsonData/sidebar_routes.json'
import {useSelector} from "react-redux";

const SidebarItem = props => {

    const themeReducer = useSelector(state => state.ThemeReducer)

    const active = props.active ? 'active' : ''

    return (
        <div className={`sidebar__item ${themeReducer.word}`}>
            <div className={`sidebar__item-inner ${active}`}>
                <i className={props.icon}></i>
                <span>
                    {props.title}
                </span>
            </div>
        </div>
    )
}

const Sidebar = () => {

    const history = useHistory()
    const themeReducer = useSelector(state => state.ThemeReducer)
    const [activeItem, setActiveItem] = React.useState('')

    React.useEffect(() => {
        history.listen((location) => {
            let url = location.pathname.split('/')[1]
            setActiveItem(sidebar_items.findIndex(item => item.route === `/${url}`))
        });
        let url = history.location.pathname.split('/')[1]
        setActiveItem(sidebar_items.findIndex(item => item.route === `/${url}`))
    }, [])

    return (
        <div className={`sidebar`}>
            <div className="sidebar__logo flex items-center">
                <img src={logo} alt="company logo"/>
                <h2 className={`pt-4 ml-2 ${themeReducer.word}`}>Gym Admin</h2>
            </div>
            {
                sidebar_items.map((item, index) => (
                    <Link to={item.route} key={index}>
                        <SidebarItem
                            title={item.display_name}
                            icon={item.icon}
                            active={index === activeItem}
                        />
                    </Link>
                ))
            }
        </div>
    )
}

export default Sidebar
