import {Button} from "antd";

export const BASE_URL = process.env.REACT_APP_SERVER_URL;
export const DNS_IMAGE = process.env.REACT_APP_DNS_IMAGE;
export const PREFIX_FRONT_URL = '';
export const CONTEXT_PATH = '/api';
export const TIME_OF_DEBOUNCE = 300;
export const PRIMARY_COLOR = '#FD7E14';
export const BACKGROUND_COLOR = 'white';
export const SECOND_COLOR = '#E6E6FA';
export const CURRENCY_UNIT = 'MT';

console.log(BASE_URL)

export const ROLES = [
    'EDITOR',
    'ADMIN'
]

export const NEWS_STATUS = [
    {
        id: -1,
        code: "all"
    },
    {
        id: 1,
        code: "draft",
        button: "Submit"
    },
    {
        id: 2,
        code: "submitted",
        button: "Approve"
    },
    {
        id: 3,
        code: "approved",
        button: "Publish"
    },
    {
        id: 4,
        code: "published",
        button: "Disable"
    },
    {
        id: 5,
        code: "expired"
    },
    {
        id: 0,
        code: 'invalid'
    },
]

export const defaultMenus = [
    {
        code: "CUSTOMERS",
        title: "Customers"
    },
    {
        code: "EDIT",
        title: "Edit News"
    },
    {
        code: "CATEGORIES",
        title: "Categories"
    },
    {
        code: "REGISTER",
        title: "Register"
    },
    {
        code: "ASSET",
        title: "Asset"
    },
    {
        code: "LESSON",
        title: "Lesson",
    },
    {
        code: "UNIT",
        title: "Unit",
    },
    {
        code: "TRAINER",
        title: "Trainer",
    }
]

export const USER_STATUS = [
    {
        id: "-1",
        code: "all",
    },
    {
        id: "ADMIN",
        code: "admin"
    },
    {
        id: "USER",
        code: "user"
    }
]

export const COMMON_STATUS = [
    {
        id: -1,
        code: "all",
    },
    {
        id: 0,
        code: "inactive",
    },
    {
        id: 1,
        code: "active",
    }
]

const CONTROLS = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
};

export default {
    CONTROLS,
    EN: 'en',
    VI: 'vi',
    TYPE_ERROR: 'error',
    PAGE_SIZE: 10,
    GAME_CODE: process.env.REACT_APP_GAME_CODE,
};
