import React from "react";
import Lottie from 'react-lottie';
import animationData from "../../assets/JsonData/69867-4-dots.json"

function Loading(){

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    return (
        <div style={{
            width: "100%",
            height: "100%"
        }} className="flex items-center justify-center">
            <Lottie options={defaultOptions}
                    height={200}
                    width={200}/>
        </div>
    )
}

export default Loading;
