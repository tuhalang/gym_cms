package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.dto.AccountDto;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {

    private final UserService userService;

    @PostMapping("/signIn")
    public ResponseDto signIn(@RequestBody AccountDto accountDto){
        return userService.signIn(accountDto);
    }

    @PostMapping("/signUp")
    public ResponseDto signUp(@RequestBody AccountDto accountDto){
        return userService.signUp(accountDto);
    }
}
