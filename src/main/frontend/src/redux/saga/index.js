import {call, put, takeLatest} from 'redux-saga/effects'
import {
    CREATE_ASSET,
    GET_MENUS,
    GET_MENUS_SUCCESS,
    LIST_ASSET,
    LOGIN,
    REGISTER,
    LIST_USERS,
    SAVE_DRAFT,
    UPDATE_DRAFT,
    LIST_CATEGORIES,
    CREATE_CATEGORIES,
    UPDATE_CATEGORIES,
    UPDATE_USER,
    SEARCH_CATEGORIES,
    LIST_TAGS,
    TREE_CATEGORIES,
    LESSON,
    CREATE_LESSON,
    UPDATE_LESSON,
    UNIT,
    CREATE_UNIT,
    UPDATE_UNIT,
    TRAINER, CREATE_TRAINER, UPDATE_TRAINER, DELETE_ASSET, LOAD_MORE_SUCCEED, LOAD_MORE
} from "../actions/action_types";
import getFactory from "../../api"
import {message} from "antd";
import {
    listAssetSuccess,
    loginSuccess,
    listUsersSuccess,
    listCategoriesSuccess,
    searchCategoriesSuccess,
    listTagsSuccess,
    treeCategoriesSuccess, lessonSuccess, trainerSuccess, unitSuccess
} from "../actions";
import _ from "lodash"
import {defaultMenus} from "../../utils/constant";

const api = getFactory("user")
const apiAsset = getFactory("asset")
const apiNew = getFactory("news")
const apiCategory = getFactory("category")
const apiLesson = getFactory("lesson")
const apiUnit = getFactory("unit")
const apiTrainer = getFactory("trainer")

function* login(action) {
    console.log("======== login ========", action)
    try {
        const res = yield call((params) => api.login(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            const {token, roles} = data
            console.log(data, "data")
            localStorage.setItem("accessToken", token)
            localStorage.setItem("roles", roles || null)
            localStorage.setItem("username", action.params.username || null)
            delete data.token
            yield put(loginSuccess())
        }
    } catch (e) {
        console.log(e)
    }
}

function* register(action) {
    console.log("======== register ========", action)
    try {
        const res = yield call((params) => api.register(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
            // yield put(registerSuccess())
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listUsers(action) {
    console.log("======== listUsers ========", action)
    try {
        const res = yield call((params) => api.list(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            console.log(data)
            yield put(listUsersSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    }
}

function* deleteAsset(action) {
    console.log("======== deleteAsset ========", action)
    try{
        const res = yield call((params) => apiAsset.deleteAsset(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            console.log("callback")
            if (_.isFunction(action.callback)) action.callback()
        }
    }catch (e) {
        console.log(e)
    }
}

function* listAsset(action) {
    console.log("======== listAsset ========", action)
    try {
        const res = yield call((params) => apiAsset.list(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            console.log(data)
            yield put(listAssetSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    }
}

function* loadMore(action) {
    console.log("======== loadMore ========", action)
    try {
        const res = yield call((params) => apiAsset.list(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put({
                type: LOAD_MORE_SUCCEED,
                payload: data
            })
        }
    } catch (e) {
        console.log(e)
    }
}

function* createAsset(action) {
    console.log("======== createAsset ========", action)
    try {
        const res = yield call((params) => apiAsset.create(params), action.params)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            console.log("callback")
            if (_.isFunction(action.callback)) action.callback()
        }
    } catch (e) {
        console.log(e)
    }
}

function* getMenus(action) {
    try {
        const menus = localStorage.getItem("menus")
        let data = defaultMenus
        if (menus) {
            data = JSON.parse(menus)
        } else {
            // data = await apiUser.getMenus()
            localStorage.setItem("menus", JSON.stringify(data))
        }
        if (data.length > 0) {
            yield put({
                type: GET_MENUS_SUCCESS,
                payload: data,
            })
        }
    } catch (e) {
        console.log(e)
    }
}

////////////////////////////////////////////  NEWS //////////////////////////////////////////////////////////////////////////////////////

function* saveNewsDraft(action) {
    console.log("======== saveNewsDraft ========", action)
    try {
        const res = yield call((payload) => apiNew.draft(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            console.log("callback")
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* updateNewsDraft(action) {
    console.log("======== updateNewsDraft ========", action)
    try {
        const res = yield call((payload) => apiNew.updateDraft(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            console.log("callback")
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listCategories(action) {
    console.log("======== listCategories ========", action)
    try {
        const res = yield call((payload) => apiCategory.list(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            if (_.isFunction(action.callbackSuccess)) action.callbackSuccess(data)
            yield put(listCategoriesSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* searchCategories(action) {
    console.log("======== searchCategories ========", action)
    try {
        const res = yield call((payload) => apiCategory.search(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put(searchCategoriesSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* createCategory(action) {
    console.log("======== createCategory ========", action)
    try {
        const res = yield call((payload) => apiCategory.create(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
            if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* updateCategory(action) {
    console.log("======== updateCategory ========", action)
    try {
        const res = yield call((payload) => apiCategory.update(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
            if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* updateUser(action) {
    console.log("======== updateUser ========", action)
    try {
        const res = yield call((payload) => api.update(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
            // yield put(listCategoriesSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listTags(action) {
    console.log("======== listTags ========", action)
    try {
        const res = yield call((payload) => apiCategory.listTags(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put(listTagsSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* treeCategories(action) {
    console.log("======== treeCategories ========", action)
    try {
        const res = yield call((payload) => apiCategory.tree(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put(treeCategoriesSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* lesson(action) {
    console.log("======== lesson ========", action)
    try {
        const res = yield call((payload) => apiLesson.list(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put(lessonSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* createLesson(action) {
    console.log("======== createLesson ========", action)
    try {
        const res = yield call((payload) => apiLesson.create(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* updateLesson(action) {
    console.log("======== updateLesson ========", action)
    try {
        const res = yield call((payload) => apiLesson.update(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* unit(action) {
    console.log("======== unit ========", action)
    try {
        const res = yield call((payload) => apiUnit.list(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put(unitSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* createUnit(action) {
    console.log("======== createUnit ========", action)
    try {
        const res = yield call((payload) => apiUnit.create(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* updateUnit(action) {
    console.log("======== updateUnit ========", action)
    try {
        const res = yield call((payload) => apiUnit.update(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* trainer(action) {
    console.log("======== trainer ========", action)
    try {
        const res = yield call((payload) => apiTrainer.list(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            yield put(trainerSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* createTrainer(action) {
    console.log("======== createTrainer ========", action)
    try {
        const res = yield call((payload) => apiTrainer.create(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* updateTrainer(action) {
    console.log("======== updateTrainer ========", action)
    try {
        const res = yield call((payload) => apiTrainer.update(payload), action.payload)
        const {
            errorCode,
            data,
        } = res
        if (errorCode != "0") {
            message.error(res.message)
        } else {
            message.success(res.message)
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* rootSaga() {
    yield takeLatest(LOGIN, login);
    yield takeLatest(GET_MENUS, getMenus);
    yield takeLatest(REGISTER, register);
    yield takeLatest(LIST_ASSET, listAsset);
    yield takeLatest(CREATE_ASSET, createAsset);
    yield takeLatest(LIST_USERS, listUsers);
    yield takeLatest(SAVE_DRAFT, saveNewsDraft);
    yield takeLatest(UPDATE_DRAFT, updateNewsDraft);
    yield takeLatest(LIST_CATEGORIES, listCategories);
    yield takeLatest(CREATE_CATEGORIES, createCategory);
    yield takeLatest(UPDATE_CATEGORIES, updateCategory);
    yield takeLatest(UPDATE_USER, updateUser);
    yield takeLatest(SEARCH_CATEGORIES, searchCategories);
    yield takeLatest(LIST_TAGS, listTags);
    yield takeLatest(TREE_CATEGORIES, treeCategories);
    yield takeLatest(LESSON, lesson);
    yield takeLatest(CREATE_LESSON, createLesson);
    yield takeLatest(UPDATE_LESSON, updateLesson);
    yield takeLatest(UNIT, unit);
    yield takeLatest(CREATE_UNIT, createUnit);
    yield takeLatest(UPDATE_UNIT, updateUnit);
    yield takeLatest(TRAINER, trainer);
    yield takeLatest(CREATE_TRAINER, createTrainer);
    yield takeLatest(UPDATE_TRAINER, updateTrainer);
    yield takeLatest(DELETE_ASSET, deleteAsset);
    yield takeLatest(LOAD_MORE, loadMore);
}

export default rootSaga;
