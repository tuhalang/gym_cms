import Client from "../client/Client"

const resource = "/secure/categories"

const list = (params) => {
    return Client.get(`${resource}`, {params})
}

const create = (data) => {
    return Client.post(`${resource}`, data)
}

const update = (data) => {
    return Client.put(`${resource}`, data)
}

const listTags = (params) => {
    return Client.get(`/secure/tags`, {params})
}

const search = (params) => {
    return Client.get(`${resource}/search`, {params})
}

const tree = (params) => {
    return Client.get(`${resource}/tree`, {params})
}

const categoryRepository = {
    list,
    create,
    update,
    listTags,
    search,
    tree,
}
export default categoryRepository
