import React, {useRef} from "react"
import {
    Row,
    Col,
    Input,
    Tabs,
    DatePicker, Space, Button, Select, AutoComplete, Tooltip,
} from "antd"
import {ClearOutlined, SearchOutlined} from "@ant-design/icons"
import moment from "moment"
import {withTranslation} from "react-i18next"
import {paramsUrl, selectedDate} from "../../components/function";
import {COMMON_STATUS, NEWS_STATUS} from "../../utils/constant";
import {useDispatch, useSelector} from "react-redux";
import {approveNews, expireNews, lesson, publishNews, searchCategories, submitNews} from "../../redux/actions";
import {Link, useHistory} from "react-router-dom";
import EditUnit from "../../components/modal/EditUnit";

const {TabPane} = Tabs
const dfColFilter = {sm: 12, md: 12, lg: 8, xl: 8}

const Filter = ({onFilter, filter, onFetch}) => {
    const dispatch = useDispatch()
    const lessonState = useSelector(state => state.lesson)
    const [editing, setEditing] = React.useState(false)
    const __filter = useRef({
        ...paramsUrl.get()
    })

    function onChangeFilter(key, value) {
        __filter.current[key] = value
        onFilter(__filter.current)
    }

    function handlerSubmit() {
        onFetch()
    }

    const {name} = filter

    const onSelect = (data, {id}) => {
        onChangeFilter("lessonId", id)
    };
    const onSearch = (data) => {
        dispatch(lesson({
            name: data,
            page: 1,
            size: 10,
        }))
    };

    React.useEffect(() => {
        onSearch("")
    }, [])

    const {items} = lessonState

    return (
        <div className="items-center m-4 card-filter">
            <Row>
                <Tabs
                    className="w-full"
                    defaultActiveKey={filter ? filter.status : "1"}
                    onChange={(value) =>
                        onChangeFilter("status", value)
                    }
                >
                    {COMMON_STATUS.length > 0 &&
                    COMMON_STATUS.map(({id, code}) => {
                        return (
                            <TabPane
                                tab={<span className="capitalize">{code}</span>}
                                key={id.toString()}
                            />
                        )
                    })}
                </Tabs>
            </Row>
            <div className="flex justify-between">
                <Row gutter={12}>
                    <Col {...dfColFilter} className="mb-4">
                        <Input
                            prefix={<SearchOutlined/>}
                            placeholder="Name..."
                            defaultValue={name}
                            onChange={({target: {value}}) =>
                                onChangeFilter("name", value ? value : "")
                            }
                        />
                    </Col>
                    <Col {...dfColFilter} className="mb-4">
                        <AutoComplete
                            className="w-full"
                            options={items.map(item => ({
                                label: item.nameEn,
                                value: item.nameEn,
                                id: item.id,
                            }))}
                            clearIcon={<ClearOutlined onClick={() => onChangeFilter("lessonId", null)}/>}
                            allowClear={true}
                            onSelect={onSelect}
                            onSearch={onSearch}
                            placeholder={'--Lesson--'}
                            value={filter.lessonId && items.find(item => item.id === filter.lessonId) ? items.find(item => item.id === filter.lessonId).nameEn : ""}
                        />
                    </Col>
                </Row>
                <Space className="items-start">
                    <Tooltip title="search">
                        <Button type="primary" shape="circle" icon={<SearchOutlined/>} onClick={handlerSubmit}/>
                    </Tooltip>
                    <Button type={"primary"} onClick={() => setEditing(true)}>Create</Button>
                </Space>
            </div>
            <EditUnit
                visible={editing}
                onClose={() => {
                    setEditing(false)
                }}
                defaultValue={undefined}
            />
        </div>
    )
}

export default withTranslation()(Filter)
