package com.viettel.gym_cms.controller;

import com.viettel.gym_cms.domain.Trainer;
import com.viettel.gym_cms.dto.ResponseDto;
import com.viettel.gym_cms.service.TrainerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class TrainerController {

    private final TrainerService trainerService;

    @RequestMapping(value = "/trainer", method = RequestMethod.POST)
    ResponseDto create(@RequestBody Trainer trainer){
        return trainerService.create(trainer);
    }

    @RequestMapping(value = "/trainer", method = RequestMethod.PUT)
    ResponseDto update(@RequestBody Trainer trainer){
        return trainerService.update(trainer);
    }

    @RequestMapping(value = "/trainer", method = RequestMethod.GET)
    ResponseDto getAll(@RequestParam Integer page,
                       @RequestParam Integer size,
                       @RequestParam(required = false) String name,
                       @RequestParam(required = false) Integer status){
        Pageable pageable = PageRequest.of(page-1, size, Sort.by("nameEn").ascending());
        return trainerService.getAll(pageable, status, name);
    }
}
