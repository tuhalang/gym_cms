import React, {useState} from "react"
import {ROOT_FOLDER} from "../hooks/useFolder";
import {Button, Form, Input, Modal} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {createAsset, listAsset} from "../../redux/actions";

export default function AddFolderButton({currentFolder}) {
    const [open, setOpen] = useState(false)
    const [name, setName] = useState("")

    const {folder} = useSelector(state => state.common)

    const dispatch = useDispatch()

    function closeModal() {
        setOpen(false)
    }

    function openModal() {
        setOpen(true)
    }

    function handleSubmit() {
        dispatch(createAsset({
            file: null,
            name: name,
            parentId: currentFolder ? currentFolder.id ? currentFolder.id : "" : "",
            type: 1,
            callback: () => {
                dispatch(listAsset({
                    parentId: folder.id,
                    name: "",
                    page: 1,
                    size: 40,
                    status: 1,
                }))
            }
        }))
        // if (currentFolder !== ROOT_FOLDER) {
        //     path.push({name: currentFolder.name, id: currentFolder.id})
        // }

        // database.folders.add({
        //     name,
        //     path,
        //     createdAt: getCurrentTimestamp(),
        //     parentId: currentFolder.id,
        //     userId: currentUser.uid
        // })

        setName("")
        closeModal()
    }

    return (
        <>
            <Button type={'dashed'} onClick={openModal} className="btn btn-outline-success btn-sm">
                <i className='bx bxs-folder-plus'></i>
            </Button>

            <Modal
                visible={open}
                onCancel={closeModal}
                onOk={handleSubmit}
                closable={false}
            >
                <Input onChange={e => setName(e.target.value)} value={name} placeholder={"Folder name input"}/>
            </Modal>
        </>
    )
}
